local _M = {}
local http = require "resty.http"
local copy_time = ngx.time() .. '000'

function _M.request_report(forwarded_for, request_type, path)
  local httpc = http.new()
  local res, err = httpc:request_uri(
                       "http://influxdb.default.svc.cluster.local:8086/write?db=blog_stat",
                       {
        method = "POST",
        body = 'request url="' .. path .. '",type="' .. request_type .. '",ip="' ..
            forwarded_for .. '",copy_time=' .. copy_time
      })

  if not res then
    ngx.log(ngx.ERR, err)
    return
  end

  ngx.log(ngx.ERR, res.status, ' ', res.body)
end

function _M.rate_limit_report(forwarded_for, path)
  local httpc = http.new()
  local res, err = httpc:request_uri(
                       "http://influxdb.default.svc.cluster.local:8086/write?db=blog_stat",
                       {
        method = "POST",
        body = 'rate_limit url="' .. path .. '",ip="' .. forwarded_for .. '"'
      })

  if not res then
    ngx.log(ngx.ERR, err)
    return
  end

  ngx.log(ngx.ERR, res.status, ' ', res.body)
end

return _M
