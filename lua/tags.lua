local template = require"resty.template".new({
  root = "/root/blog"
  -- location = "/root/blog",
})
local json = require("cjson")
local key = string.sub(ngx.var.request_uri, 6)
local global_dict = ngx.shared.global_dict;
local tags_json = global_dict:get('json')
local json_obj = json.decode(tags_json)
template.render("tags.html", {tag_list = json_obj[key]})
