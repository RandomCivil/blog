function Set(list)
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set
end

function InitRedis()
  local redis = require "resty.redis"
  local red = redis:new()
  red:set_timeout(1000)

  local ok, err = red:connect("redis.default.svc.cluster.local", 6379)
  if not ok then
    ngx.log(ngx.ERR, "failed to connect: ", err)
    return nil
  end

  return red
end

ngx.log(ngx.ERR, ngx.var.proxy_protocol_addr, ' ', ngx.var.http_x_forwarded_for)

local global_dict = ngx.shared.global_dict;
local global_data = require '/root/lua/global'
local blacklist_ips = global_data.get_blacklist_ips()
local whitelist_ips = global_data.get_whitelist_ips()

local cidr = require("libcidr-ffi")
local proxy_protocol_addr = ngx.var.proxy_protocol_addr
local forwarded_for = ngx.var.http_x_forwarded_for

if forwarded_for == nil then forwarded_for = 'nil' end

for i, v in ipairs(blacklist_ips) do
  if cidr.contains(cidr.from_str(v), cidr.from_str(forwarded_for)) then
    ngx.log(ngx.ERR, 'baidu ', forwarded_for, ' match ', v)
    return ngx.exit(ngx.HTTP_FORBIDDEN)
  end
end

local is_from_google = cidr.contains(cidr.from_str('66.249.0.0/16'),
                                     cidr.from_str(forwarded_for))
ngx.log(ngx.ERR, 'is_from_google: ', is_from_google)

local user_agent = ngx.var.http_user_agent
local is_google_bot = string.match(user_agent, 'Googlebot/2.1;')
ngx.log(ngx.ERR, 'is_google_bot: ', is_google_bot, ' ', user_agent)

ngx.ctx.is_from_google = is_from_google
ngx.ctx.is_google_bot = is_google_bot

local is_google_request = is_from_google and is_google_bot
local request_type
if is_google_request then
  request_type = 'google'
else
  request_type = 'other'
end

local path = ngx.re.gsub(ngx.var.request_uri, "[.|/]html", "")
local black_path_list = Set {'/title_path.json', '/increase'}
if black_path_list[path] then return end

for i, v in ipairs(whitelist_ips) do
  if cidr.contains(cidr.from_str(v), cidr.from_str(forwarded_for)) then return end
end

local influxdb = require '/root/lua/influxdb'
influxdb.request_report(forwarded_for, request_type, path)

ngx.log(ngx.ERR, 'stat new ', path, ' ', request_type, ' ', forwarded_for)

local red
if red == nil then red = InitRedis() end

local res, err = red:get(forwarded_for)
if not res then
  ngx.log(ngx.ERR, "failed to get forwarded_for: ", err)
  return
end

if res ~= ngx.null then
  ngx.log(ngx.ERR, "found in redis,reject ip:", forwarded_for)
  return ngx.exit(403)
end

local limit_req = require "resty.limit.req"
local lim, err = limit_req.new("my_limit_req_store", 3, 1)
if not lim then
  ngx.log(ngx.ERR, "failed to instantiate a resty.limit.req object: ", err)
  return ngx.exit(500)
end

local delay, err = lim:incoming(forwarded_for, true)
if not delay then
  if err == "rejected" then
    red:set(forwarded_for, "haha")
    red:expire(forwarded_for, 3600)
    ngx.log(ngx.ERR, 'reject ip:', forwarded_for)
    -- influxdb.rate_limit_report(forwarded_for, path)
    return ngx.exit(403)
  end

  ngx.log(ngx.ERR, "failed to limit req: ", err)
  return ngx.exit(500)
end
