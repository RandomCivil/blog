function GetIPType(ip)
  local R = {ERROR = 0, IPV4 = 1, IPV6 = 2, STRING = 3}
  if type(ip) ~= "string" then return R.ERROR end

  -- check for format 1.11.111.111 for ipv4
  local chunks = {ip:match("^(%d+)%.(%d+)%.(%d+)%.(%d+)$")}
  if #chunks == 4 then
    for _, v in pairs(chunks) do
      if tonumber(v) > 255 then return R.STRING end
    end
    return R.IPV4
  end

  -- check for ipv6 format, should be 8 'chunks' of numbers/letters
  -- without leading/trailing chars
  -- or fewer than 8 chunks, but with only one `::` group
  local chunks = {ip:match("^" .. (("([a-fA-F0-9]*):"):rep(8):gsub(":$", "$")))}
  if #chunks == 8 or #chunks < 8 and ip:match('::') and
      not ip:gsub("::", "", 1):match('::') then
    for _, v in pairs(chunks) do
      if #v > 0 and tonumber(v, 16) > 65535 then return R.STRING end
    end
    return R.IPV6
  end

  return R.STRING
end

local cidr = require("libcidr-ffi")
local global_data = require '/root/lua/global'
local whitelist_ips = global_data.get_whitelist_ips()
local forwarded_for = ngx.var.http_x_forwarded_for
local flag = false

local ip_type = GetIPType(forwarded_for)
if ip_type == 2 then return end

for i, v in ipairs(whitelist_ips) do
  if cidr.contains(cidr.from_str(v), cidr.from_str(forwarded_for)) then
    flag = true
  end
end

if not flag then
  ngx.log(ngx.ERR, "proxy forbid: ", forwarded_for)
  return ngx.exit(ngx.HTTP_NOT_FOUND)
end
