ARG BUILD_IMAGE_URL
FROM $BUILD_IMAGE_URL as build

FROM theviper/docker-openresty:latest
COPY --from=build /build/nginx.conf /usr/local/openresty/nginx/conf
COPY --from=build /build/lua /root/lua
COPY --from=build /build/blog /root/blog
