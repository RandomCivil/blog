## feature

### gulp

- 开发环境热加载+预览，且只对修改过的文件进行相应编译
- 模板页修改,则更新所有markdown生成的html,热加载当前页面
- 编译markdown,less
- 根据模板组装页面
- 打包js,css,并重命名为md5格式，替换生成页面相应位置
- 自定义标签插件

### gitlab CI/CD

- docker executor
- node_modules cache in docker
- multi-stage builds
- ansible+kubernetes deploy
