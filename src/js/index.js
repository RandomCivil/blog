//bash,json,nginx,python,lua,go,yaml,diff,vim,js,html,sql,Dockerfile
hljs.initHighlightingOnLoad()
hljs.initLineNumbersOnLoad()

var title_input=document.querySelector('#title-search'),
    content_input=document.querySelector('#content-search'),
    request = new XMLHttpRequest(),
    fire=false,
    ul=document.querySelector('header ul')

function increase_score(title,path){
    request.open('POST', 'https://theviper.xyz/search/increase', true)
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')

    request.onload = function() {
        if (request.status == 200) {
            window.location.href='https://theviper.xyz/'+path+'.html'
        }
    }

    request.send('title='+title)
}

title_input.addEventListener('keydown', function(){
    if(title_input.value==''){
        ul.innerHTML=''
        fire=false
        return
    }

    if(fire){
        return
    }

    fire=true

    setTimeout(function(){
        var val=title_input.value
        if(val==''){
            ul.innerHTML=''
            fire=false
            return
        }

        request.open('GET', 'https://theviper.xyz/search/'+val, true)
        request.onload = function() {
            if (this.readyState == this.DONE && request.status == 200) {
                ul.innerHTML=''
                var data = JSON.parse(request.responseText),
                    html=''
                for(var i=0;i<data.length;i++){
                    html+="<li><a href='https://theviper.xyz/"+data[i].Path+".html' target='_blank'>"+data[i].Title+"</a></li>"
                    //var li = document.createElement("li"),
                        //title=data[i].Title,
                        //path=data[i].Path;
                    //li.innerHTML="<a href='javascript:;'>"+title+"</a>";
                    //(function(title,path){
                        //li.addEventListener('click', function(){
                            //increase_score(title,path)
                        //})
                    //})(title,path)
                    //ul.appendChild(li)
                }
                ul.innerHTML=html
            }
        }

        fire=false
        request.send()
    },1000)
})

//content_input.addEventListener('keydown', function(e){
    //if(e.which==13){
        //window.location.href='https://theviper.xyz/es/'+content_input.value
    //}
//})
