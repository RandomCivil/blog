<!-- build:title -->命令行查看代码sloc<!-- /build:title -->
<!-- build:tags -->linux<!-- /build:tags -->
<!-- build:content -->
[sloc](https://en.wikipedia.org/wiki/Source_lines_of_code)   
平时我都用[fd](https://github.com/sharkdp/fd)代替find命令

- 查看每个go文件行数及总行数

```bash
fd go$ | xargs wc -l
```

- 排除测试文件

```bash
fd -E "*_test.go" go$ | xargs wc -l
```

- 不计空行情况

```bash
fd go$ | xargs cat | grep -P '\S' | wc -l
```

- 不计空行情况+不计代码注释

```bash
fd go$ | xargs cat | grep -P '\S' | sed '/^\s*\/\//d' | wc -l
```

也可以使用[cloc](https://github.com/AlDanial/cloc)

```bash
cloc your_dir
```

- 排除测试文件

```bash
cloc --not-match-f='_test' your_dir
```
<!-- /build:content -->
