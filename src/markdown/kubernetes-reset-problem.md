<!-- build:title -->kubernetes reset problem<!-- /build:title -->
<!-- build:tags -->kubernetes<!-- /build:tags -->
<!-- build:content -->
## 问题

kubernetes node节点`reset`重置后,又`kubeadm join`.部署时,出现错误

```
Error adding network: failed to allocate for range 0: no IP addresses available in range set: 10.244.1.1-10.244.1.254
Error while adding to cni network: failed to allocate for range 0: no IP addresses available in range set: 10.244.1.1-10.244.1.254
```

导致部署时,pod的状态一直是pending

## 原因

node节点`reset`时没有删除相应网络配置

## 解决

node节点

```bash
kubeadm reset
systemctl stop kubelet
systemctl stop docker
rm -rf /var/lib/cni/
rm -rf /var/lib/kubelet/*
rm -rf /etc/cni/

ifconfig cni0 down
ifconfig flannel.1 down
ip link delete cni0
ip link delete flannel.1

systemctl restart kubelet
systemctl restart docker
```

最后重新`kubeadm join`就行了

## master节点reset

执行

```
kubeadm reset
kubeadm init --pod-network-cidr=10.244.0.0/16
```

之后,需要重新安装`kube-flannel-rbac`,`kube-flannel`

```
kubectl create -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml
kubectl create -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml
```

<!-- /build:content -->
