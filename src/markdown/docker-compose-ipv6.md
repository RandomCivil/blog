<!-- build:title -->docker compose支持IPv6<!-- /build:title -->
<!-- build:tags -->iptables,docker<!-- /build:tags -->
<!-- build:content -->

环境：fedora 32,docker 19.03.8

## docker compose支持IPv6

- network_mode: "host"时，IPv6支持取决于宿主机
- 不需要配置IPv6转发

/etc/sysctl.conf

```
net.ipv6.conf.default.forwarding=1
net.ipv6.conf.all.forwarding=1
```

- 不需要设置docker daemon为

```json
{
    "fixed-cidr-v6": "2001:db8:1::/64", 
    "ipv6": true
}
```

- docker compose配置文件**version需要2.x**

```yaml
version: '2.4'
services:
  quic:
    container_name: "quic"
    image: "nginx:latest"
    ...
    ports:
      - "3000:3000"
    networks:
      app_net:
        ipv6_address: 2001:3200:3200::20
networks:
  app_net:
    enable_ipv6: true
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 2001:3200:3200::/64
          gateway: 2001:3200:3200::1
```

> If IPv6 addressing is desired, the enable_ipv6 option must be set, and you must use a version 2.x Compose file

docker inspect创建的network

```json
[
    {
        "Name": "root_app_net",
        "Id": "5520dc0d52481e08a17e771758830e64ea2cc1254a93aee934287081ed23b2db",
        "Created": "2020-05-03T01:33:53.317655914Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": true,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.19.0.0/16",
                    "Gateway": "172.19.0.1"
                },
                {
                    "Subnet": "2001:3200:3200::/64",
                    "Gateway": "2001:3200:3200::1"
                }
            ]
        },
        "Internal": false,
        "Attachable": true,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "48f495b4fa2e7f292384f9342296c34c0cb91bdfeda1a67332213e1814f971b9": {
                "Name": "kcp",
                "EndpointID": "66cf6e3401e6375051e9911b2cb5fbfb47f20cc9d858bf022f18d63cdd98c678",
                "MacAddress": "02:42:ac:13:00:02",
                "IPv4Address": "172.19.0.2/16",
                "IPv6Address": "2001:3200:3200::20/64"
            }
        },
        "Options": {},
        "Labels": {
            "com.docker.compose.network": "app_net",
            "com.docker.compose.project": "root",
            "com.docker.compose.version": "1.25.5"
        }
    }
]
```

"EnableIPv6": true,IPv6Address是networks.app_net.ipv6_address设置的2001:3200:3200::20

- 需要设置`ip6tables`

```bash
ip6tables -t nat -I POSTROUTING -j MASQUERADE
```

MASQUERADE会自动读取eth0现在的ip地址然后做SNAT出去.如果没设置，会i/o timeout

---
不定期更新
<!-- /build:content -->
