<!-- build:title -->openresty定制prometheus metrics监控nginx(2)<!-- /build:title -->
<!-- build:tags -->openresty,prometheus,nginx,grafana<!-- /build:tags -->
<!-- build:content -->
[上一篇](/monitor-nginx-with-openresty-and-prometheus-1.html)讲到prometheus alert,事实上，这里面有个很尴尬的问题,那就是[部署](/gitlab-ci-cd-config.html)时，counter会变成`no data`,见下图

![](../images/prometheus-alert/1.png)

这会导致之前的alert rule

```
sum(increase(nginx_http_google_requests_total[2m])) by (path) >0
```

在第一次请求时,无法探测此时counter的变化,见下图

![](../images/prometheus-alert/2.png)

只能探测第一次请求后，counter的变化,见下图

![](../images/prometheus-alert/4.png)

google了半天,找到这篇[Existential issues with metrics](https://www.robustperception.io/existential-issues-with-metrics/),文章最后提到一个可以探测任何增长变化的alert rule

```
  # We saw a normal increase.   
  rate(my_counter_total[5m]) > 0   
  or 
  (  # If the value is non-zero, the counter didn't exist a minute ago, 
     # and the target was up a minute ago.
     (my_counter_total != 0 unless my_counter_total offset 1m) 
   and 
     (up offset 1m == 1)
  )
```

其实可以不用这么复杂,稍微修改下,就可以满足要求,见下图

```
sum(increase(nginx_http_requests_total[2m])) by (path) or sum(nginx_http_requests_total) by (path)
```

![](../images/prometheus-alert/3.png)

这里用了[or关键字](https://prometheus.io/docs/prometheus/latest/querying/operators/#logical-set-binary-operators),表示
- 如果or的前面部分有值(0,1,2,...),即第一次请求后的情况,就取前面increase部分
- 否则,即第一次请求的情况,上面第二张图右边部分,就取后面total部分

<!-- /build:content -->
