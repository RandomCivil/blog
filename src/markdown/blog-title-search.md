<!-- build:title -->博客开始支持标题搜索<!-- /build:title -->
<!-- build:tags -->algorithm,go,blog<!-- /build:tags -->
<!-- build:content -->

## 功能

- 支持前缀搜索
- 前缀搜索支持拼音输入
- 支持分词搜索

## redis sorted set方式

将标题分解为前缀形式，保存在`sorted set`,比如`nghttpx tips`和`nginx pass real ip`

对nghttpx tips

```
ZADD 'n' 1 'nghttpx tips'
ZADD 'ng' 1 'nghttpx tips'
ZADD 'ngh' 1 'nghttpx tips'
ZADD 'nght' 1 'nghttpx tips'
...
```

对nginx pass real ip

```
ZADD 'n' 1 'nginx pass real ip'
ZADD 'ng' 1 'nginx pass real ip'
ZADD 'ngi' 1 'nginx pass real ip'
ZADD 'ngin' 1 'nginx pass real ip'
...
```

搜索的时候,比如`ng`

```
ZRANGE 'ng' 0 -1
```

因为key是`ng`的sorted set是这样保存的

```
key value
ng  ['nghttpx tips','nginx pass real ip']
```

## 汉字->拼音

这里使用[go-pinyin](https://github.com/mozillazg/go-pinyin),注意转换的拼音要替换原来的汉字

```go
import (
	"strings"

	pinyin "github.com/mozillazg/go-pinyin"
)

func Convert(str string) []string {
	result := []string{}
	p := pinyin.NewArgs()
	for i := 0; i < len(str); i++ {
		if str[i] == ' ' {
			continue
		}

		var key string
		if len(string(str[i])) > 1 && len(string(str[i:])) > 1 {
			pinyin_str := pinyin.Pinyin(string(str[i:i+3]), p)
			key = str[:i] + pinyin_str[0][0]
			str = key + str[i+3:]
		} else {
			key = strings.ToLower(str[:i+1])
		}
		key = strings.Replace(str[:i+1], " ", "", -1)
		result = append(result, key)
	}
	return result
}
```

redis客户端用的是[radix](https://github.com/mediocregopher/radix)

```go
...
for title, path := range data {
    prefixList := Convert(title)
	for _, key := range prefixList {
        redisClient.PipeAppend("ZADD", key, 1, title)
	}
    title_pinyin := prefixList[len(prefixList)-1]
    redisClient.PipeAppend("HSET", "title_path", title_pinyin, path)
}
redisClient.PipeResp()
```

这里用了`pipeline`批量添加到sorted set.在添加的同时，将title和path的映射保存到hash中

## trie方式

其实前缀搜索很适合使用[trie](https://zh.wikipedia.org/zh-hans/Trie)保存各词条,可以看看[Tushar Roy老师](https://www.youtube.com/user/tusharroy2525/featured)的[讲解](https://www.youtube.com/watch?v=AXjmTQ8LEoI)   
但是这样有一个问题，如果词条很多，不可能将整个trie加载到内存,这就需要将`trie持久化`，这里使用redis

根据trie的定义，节点是这样

```go
type Node struct {
	end bool
	m   map[byte]*Node
}
```

m属性的key是字符，value是下一个Node.现在将`ABC`和`ABGL`插入trie

![](../images/trie.png)

这时可以将Node看作redis的[hash类型](https://redis.io/commands#hash)

![](../images/trie-redis-hash.png)

hash类型的key用uuid标识，field-value相当于上面Node的m属性，field还是字符，value变成下一个hash类型的key(uuid).为了方便,将end标识符也加到field-value

```go
func (t *Trie) Generate(root int64, word string) int64 {
	conn := t.Pool.Get()
	defer conn.Close()

	var cur int64
	if root == 0 {
		kid := atomic.AddInt64(&keyId, 1)
		conn.Do("HSET", kid, "end", 0)
		root, cur = kid, kid
	} else {
		cur = root
	}

	rs := []rune(word)
	for i := 0; i < len(rs); i++ {
		rsf := fmt.Sprintf("%c", rs[i])
		reply, err := R.Int64(conn.Do("HGET", cur, rsf))
		if err == R.ErrNil {
			kid := atomic.AddInt64(&keyId, 1)
			conn.Do("HSET", cur, rsf, kid)
			cur = kid
		} else {
			cur = reply
		}
	}
	conn.Do("HSET", cur, "end", 1)
	return root
}
```

前缀搜索,执行下面代码的`FindBySuffix方法`。具体的,如搜索`AB`

- 找到B的下一个节点(key=uuid3)
- 开始遍历,遍历的结果(C,GL)和`AB`组合就是前缀搜索的结果(ABC,ABGL)

```go
func (t *Trie) Tranversal(root int64) []TrieResult {
	conn := t.Pool.Get()
	defer conn.Close()

	result := []TrieResult{}
	var (
		b   []byte
		dfs func(head int64)
	)
	dfs = func(head int64) {
		resp, _ := R.ByteSlices(conn.Do("HGETALL", head))
		i := 0
		for i < len(resp) {
			key := resp[i]
			val, _ := strconv.Atoi(string(resp[i+1]))
			//key=="end"
			if bytes.Compare(key, []byte{101, 110, 100}) == 0 {
				if val == 1 {
					result = append(result, TrieResult{Word: string(b)})
					i += 2
				}
			} else {
				t := b
				b = append(b, key...)
				dfs(int64(val))
				b = t
			}
			i += 2
		}
	}
	dfs(root)
	return result
}

func (t *Trie) FindBySuffix(suffix string, root int64) []TrieResult {
	conn := t.Pool.Get()
	defer conn.Close()

	result, cur, rs := []TrieResult{}, root, []rune(suffix)
	for i := 0; i < len(rs); i++ {
		rsf := fmt.Sprintf("%c", rs[i])
		next, err := R.Int64(conn.Do("HGET", cur, rsf))
		if err == R.ErrNil {
			return []TrieResult{}
		}
		cur = next
	}
	for _, v := range t.Tranversal(cur) {
		result = append(result, TrieResult{Word: suffix + v.Word})
	}
	return result
}
```

## 分词搜索

这个和上面前缀搜索-`redis sorted set`方式类似,分词使用[gojieba](https://github.com/yanyiwu/gojieba).具体的，如[博客迁移到kubernetes](/blog-migrate-to-kubernetes.html),标题分词是[博客 迁移 到 kubernetes],将每个分词保存到sorted set

```
ZADD '博客' 1 '博客迁移到kubernetes'
ZADD '迁移' 1 '博客迁移到kubernetes'
...
```

搜索和上面一样，如搜索`迁移`

```
ZRANGE '迁移' 0 -1
```

---

不定期更新
<!-- /build:content -->
