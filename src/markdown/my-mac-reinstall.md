<!-- build:title -->我的Mac重装记录<!-- /build:title -->
<!-- build:tags -->Mac<!-- /build:tags -->
<!-- build:content -->
- iterm2
- v2ray
- leanote
- chrome
- brew

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

- command line developer tools

```bash
xcode-select --install
```

## vim

```bash
git config --global http.proxy 'socks5://127.0.0.1:1080'
git config --global https.proxy 'socks5://127.0.0.1:1080'

brew install vim
```

- [vim-plug](https://github.com/junegunn/vim-plug)

```bash
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

```vim
call plug#begin('~/.vim/bundle')
Plug 'junegunn/fzf.vim'
...
call plug#end()
```

vim-go :GoInstallBinaries

- YouCompleteMe

```bash
brew install cmake ctags
cd ~/.vim/bundle/YouCompleteMe
./install.py --go-completer
```

- zsh plugins

解绑`^+<-`,`^+->`

![](../images/mac-keybind.png)

```bash
chsh -s $(which zsh)

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-history-substring-search ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-history-substring-search

plugins=(zsh-syntax-highlighting zsh-autosuggestions history-substring-search)
```

- go

```bash
export GOROOT=$HOME/dev/go
export GOPATH=$HOME/dev/gopath
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
```

- pip

~/.pip/pip.conf

```
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
```

- autojump

```bash
brew install autojump
```

.zshrc

```
[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh
```

- [bat](https://github.com/sharkdp/bat)

```bash
brew install bat
```

- [fd](https://github.com/sharkdp/fd)

```bash
brew install fd
```

- [fzf](https://github.com/junegunn/fzf)

```bash
brew install tree
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
```

- [ag](https://github.com/ggreer/the_silver_searcher)

```bash
brew install the_silver_searcher
```

---
不定期更新
<!-- /build:content -->
