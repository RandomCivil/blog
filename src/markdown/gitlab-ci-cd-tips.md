<!-- build:title -->gitlab CI/CD tips<!-- /build:title -->
<!-- build:tags -->gitlab-CI-CD,docker<!-- /build:tags -->
<!-- build:content -->
我的[配置](/gitlab-ci-cd-config.html)

## service ip

单元测试常常用到redis,这时需要

- 在`.gitlab-ci.yml`配置[services](https://gitlab.com/help/ci/docker/using_docker_images.md#define-image-and-services-from-gitlab-ciyml)

```yaml
test:
  stage: test
  ...
  services:
    - redis:alpine
  script:
    - docker pull $CACHE_IMAGE_URL || true
    - docker run $CACHE_IMAGE_URL go test ./...
```

- 代码中使用**redis替代127.0.0.1**,类似docker-compose中调用其他服务

```go
client, err := redis.Dial("tcp", "redis:6379")
```

但是真的这样做下来，发现查询dns,没有redis服务的记录

好在gitlab runner在构建过程中，会定义很多`环境变量`,如`REDIS_PORT_6379_TCP_ADDR`,它的值就是当前docker环境中redis的ip(如172.17.0.2).可以将这个值作为命令行参数传给程序

```yaml
test:
  stage: test
  ...
  services:
    - redis:alpine
  script:
    - docker pull $CACHE_IMAGE_URL || true
    - env | grep -i REDIS_PORT_6379_TCP_ADDR
    - docker run $CACHE_IMAGE_URL go test ./... -args $REDIS_PORT_6379_TCP_ADDR
```

然后程序解析并替换就行了.更多可以参考[Unable to connect to services when using docker:latest and docker:dind](https://gitlab.com/gitlab-org/gitlab-runner/issues/2691)

## elastic search service

如果service占用内存过大(如ES),会导致整个server内存超出,docker重启.对于ES,可以

- 在Dockerfile设置环境变量

```dockerfile
ENV discovery.type="single-node" \
    ES_JAVA_OPTS="-Xms300M -Xmx300M"
```

- 自定义启动service的命令,在命令中加入对内存的限制

```yaml
test:
  stage: test
  services:
    - name: registry.theviper.life/es:latest
      alias: es
      command: ["elasticsearch","...."]
```

## Protected Environment variables

![](../images/gitlab-ci-env-var.png)

若是对[Environment variables](https://gitlab.com/help/ci/variables/README#variables)设置了`Protected`,非protected branches,非protected tags的pipeline不会收到Environment variables

> Whenever a variable is protected, it would only be securely passed to pipelines running on the protected branches or protected tags. The other pipelines would not get any protected variables.

## ansible-playbook deploy

若是使用ansible-playbook部署

```yaml
---
- hosts: target_host
  remote_user: "{{user}}"
  tasks:
  ...
```

目标机器(target_host)上需要对.ssh目录及authorized_keys设置适当的**权限**，否则ansible-playbook部署无效

```bash
chmod 700 -R ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```

## error creating overlay mount to /var/lib/docker/overlay2/240ede0cb1bc430f9349152bfff98c87f2fb0145df91d22be0574d7b0dcd0c1b/merged: device or resource busy

如果CI过程中生成的镜像有点大，push时可能报上面这种错误，需要修改Runner机器的docker配置，编辑`/etc/docker/daemon.json`

```json
{
  ...
  "max-concurrent-uploads": 1
}
```

---
不定期更新
<!-- /build:content -->
