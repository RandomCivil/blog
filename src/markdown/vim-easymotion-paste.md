<!-- build:title -->vim easymotion-sn使用粘贴<!-- /build:title -->
<!-- build:tags -->vim<!-- /build:tags -->
<!-- build:content -->
[vim-easymotion](https://github.com/easymotion/vim-easymotion)的[easymotion-sn](https://github.com/easymotion/vim-easymotion#n-character-search-motion)命令和vim命令模式的`/(搜索)`一样，提供对当前文件的文本搜索.然而当我们用`Ctrl+Shift+v`,粘贴到vim命令模式时，很多vimer会发现只粘贴了复制文本的第一个字符或者什么也没粘贴.因为vim-easymotion命令模式下的粘贴需要vim支持**clipboard**或**xterm_clipboard**.   
Mac和fedora自带的vim均不支持clipboard和xterm_clipboard,需要另外安装，以支持这两个功能

- Mac可以`brew install vim`安装新的vim
- fedora可以`dnf install vim-X11`,后面用`vimx`启动
- 自己编译,则需要安装`libxt-dev`

```shell
# 让vim支持clipboard和xterm_clipboard
apt install libxt-dev
```

后面参考[ubuntu 18.04 编译vim 8.2](/ycm-vim8-2.html)进行编译

然后配置vim

```vim
map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)
autocmd VimEnter * :EMCommandLineNoreMap <C-v> <Over>(paste)
```

> <Over>(paste):Paste yanked text to the command line,Default: <C-v>

注意,虽然`<Over>(paste)`默认的快捷键是<C-v>,配置里还是要**显式**的写出来

<!-- /build:content -->
