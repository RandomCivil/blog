<!-- build:title -->dnf kubernetes tips<!-- /build:title -->
<!-- build:tags -->kubernetes,linux<!-- /build:tags -->
<!-- build:content -->
`kubernetes`更新很频繁，如果是通过`dnf(yum)`安装的话，很容易在`dnf update`的时候,一不注意，把kubernetes也一起更新了,后面学习的时候可能会因为版本原因踩坑.   
在`dnf update`的时候，不更新kubernetes,只需在`/etc/dnf/dnf.conf`加上

```
exclude=kube*
```

如果已经升级到了新版本，想降级的话，可以执行

```
dnf list --showduplicates package
```

比如`dnf list --showduplicates kubectl`

![](../images/dnf-list.png)

注意要先注释上面说的`exclude=kube*`,否则会出现Error: No matching Packages to list

找到要降级到的版本，比如`1.10.5-0`,然后

```
dnf downgrade kubectl-1.10.5-0
```

注意版本号前面有`-`

<!-- /build:content -->
