<!-- build:title -->nginx向后端传递真实客户端地址<!-- /build:title -->
<!-- build:tags -->nginx<!-- /build:tags -->
<!-- build:content -->
nginx http block用`proxy_set_header`就可以传递客户端ip   
stream block由于代理的是tcp,无法将客户端ip添加到http header里.这时就要用到[PROXY protocol](https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt)   
[PROXY protocol](https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt)简单的说，就是修改了点tcp协议，在协议里添加了一些信息，比如我们需要的客户端ip   
配置很简单,在stream里加上`proxy_protocol on`

```nginx
stream {
    ...
    server {
        listen 443;
        proxy_protocol on;
        proxy_pass 127.0.0.1:4430;
    }
}
```

既然修改了协议，backend也要支持`PROXY protocol`才行,否则无法握手成功

```nginx
http{
    server{
        listen 4430 ssl http2 fastopen=3 reuseport proxy_protocol;
    }
}
```
<!-- /build:content -->
