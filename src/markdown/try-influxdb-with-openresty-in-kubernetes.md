<!-- build:title -->kubernetes中使用influxdb+openresty<!-- /build:title -->
<!-- build:tags -->influxdb,openresty,kubernetes<!-- /build:tags -->
<!-- build:content -->
在openresty定制prometheus metrics监控nginx[1](/monitor-nginx-with-openresty-and-prometheus-1),[2](/monitor-nginx-with-openresty-and-prometheus-2)中,我们可以看到数据的变化趋势，但是如果我们想看具体的信息,把鼠标移到曲线上,只能看到由`label`聚合的数据.像下图

![](../images/prometheus-alert/4.png)

绿线由`status`聚合，只能看到不同status(status维度)的请求之和；同理，黄线由`url`聚合,只能看到不同url(url维度)的请求之和,其他信息当然看不到.

## influxdb

[influxdb](https://docs.influxdata.com/influxdb/v1.6/)是流行的时序数据库(time series database),可以作为`prometheus`的[Remote Endpoints and Storage](https://prometheus.io/docs/operating/integrations/#remote-endpoints-and-storage),用于长期存储,配置[参见](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#remote_write).   
可以将[openresty定制prometheus metrics监控nginx(1)](/monitor-nginx-with-openresty-and-prometheus-1)里供prometheus拉取的`metrics`数据也存入influxdb   

## lua-resty-http

influxdb提供[http api](https://docs.influxdata.com/influxdb/v1.6/tools/api/),在openresty中使用http client调用就行了,这里用[lua-resty-http](https://github.com/pintsized/lua-resty-http).注意openresty默认没有安装它

```lua
local request_type
if is_google_request then
  request_type='google'
else
  request_type='other'
end

local http = require "resty.http"
local httpc = http.new()
local path=ngx.re.gsub(ngx.var.request_uri, "[.|/]html", "")

local res, err = httpc:request_uri("http://influxdb-ip:8086/write?db=blog_stat", {
  method = "POST",
  body = 'request url="'..path..'",type="'..request_type..'"',
})

if not res then
  ngx.log(ngx.ERR,err)
  return
end

ngx.log(ngx.ERR,res.status,res.body)
```

这里只存了path,request_type

## influxdb quoting

influxdb http api里的`/write`,写入的数据放在[--data-binary](https://docs.influxdata.com/influxdb/v1.6/tools/api/#request-body-1),格式需要满足[Line Protocol format](https://docs.influxdata.com/influxdb/v1.6/write_protocols/line_protocol_reference/)，具体内容文档里写的很清楚，不过要注意字符串上**引号的使用**

> Never single quote field values (even if they’re strings!).   
Do double quote field values that are strings.   

不注意的话很容易出现下面错误

```
INSERT weather,location=us-midwest temperature='too warm'
ERR: {"error":"unable to parse 'weather,location=us-midwest temperature='too warm'': invalid boolean"}
```

知道这个事实后，上面lua代码里的post body就*不能写成*

```lua
body = "request url='"..path.."',type='"..request_type.."'",
```

## kubernetes dns

由于博客已经[迁移](/blog-migrate-to-kubernetes.html)到kubernetes,上面lua代码里的`influxdb-ip`就可以用`kubernetes dns`了

- nginx.conf http block添加

```nginx
resolver kube-dns.kube-system.svc.cluster.local valid=5s;
```

当然也可以`cat /etc/resolv.conf`

```
nameserver 10.96.0.10
search default.svc.cluster.local svc.cluster.local cluster.local
```

```nginx
resolver 10.96.0.10 valid=5s;
```

- lua代码使用influxdb service的dns

```lua
local res, err = httpc:request_uri("http://influxdb.default.svc.cluster.local:8086/write?db=blog_stat", {
  method = "POST",
  body = 'request url="'..path..'",type="'..request_type..'"',
})
```

注意不能像docker compose中service之间调用一样,用`service name`就能和另一个service通信.

```lua
local res, err = httpc:request_uri("http://influxdb:8086/write?db=blog_stat", {
  ...
})
```

### nginx proxy_pass dns

若是在ngx_http_proxy_module中使用kubernetes dns,需要

```nginx
        location /url {
            set $my_upstream influxdb.default.svc.cluster.local;
            proxy_pass http://${my_upstream}:8086;
        }
```

不能

```nginx
        location /url {
            proxy_pass http://influxdb.default.svc.cluster.local:8086;
        }
```

因为location block里如果

- 没有定义变量,nginx会在启动时解析proxy_pass的dns,并缓存起来
- 有定义变量,nginx会在**运行时**解析proxy_pass的dns

当然也不能

```nginx
        location /url {
            proxy_pass http://influxdb:8086;
        }
```

还有

```nginx
        location /url {
            set $my_upstream influxdb;
            proxy_pass http://${my_upstream}:8086;
        }
```

> 参考[Nginx proxy_pass directive string interpolation](https://stackoverflow.com/questions/43326913/nginx-proxy-pass-directive-string-interpolation)

---
效果

![](../images/influxdb-grafana.png)

<!-- /build:content -->
