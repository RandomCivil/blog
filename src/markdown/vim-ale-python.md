<!-- build:title -->vim ale for python<!-- /build:title -->
<!-- build:tags -->vim,python<!-- /build:tags -->
<!-- build:content -->
[vim-ale](https://github.com/w0rp/ale)

## formatter and linter

`vim-ale`对于python,支持的工具有很多,有autopep8, black, flake8, isort, mypy, prospector, pycodestyle, pydocstyle, pyls, pyre, pylint, vulture, yapf,这里面既有linter也有fixer(formatter)

[autopep8](https://github.com/hhatto/autopep8),[yapf](https://github.com/google/yapf)就是**格式化工具(formatter)**,区别是

- `autopep8`将代码格式化为符合pep8风格的代码
- `yapf`不仅能做到上面，还可以将代码格式化为google style code(通过设置--style=google)

`yapf`认为

> code that conforms to the PEP 8 guidelines may not be reformatted. But it doesn't mean that the code looks good.

与formatter不同，linter只是**提示**警告，错误。

> Available Linters: ['flake8', 'mypy', 'prospector', 'pycodestyle', 'pydocstyle', 'pyflakes', 'pylint', 'pyls', 'pyre', 'vulture']

所以不要在`ale_linters`配置formatter(autopep8,yapf)

## ale_linters

默认linter是['flake8', 'mypy', 'pylint'],指定linter的话，可以

```vim
let g:ale_linters = {
\   'go': ['vet', 'errcheck'],
\   'python': ['flake8', 'pylint'],
\}
```

像这样配置，无效!

```vim
let b:ale_linters = ['vet', 'errcheck','flake8','pylint']
```

还可以给linter传参,如忽略一些警告,比如

### flake8

```vim
let g:ale_python_flake8_options = '--ignore=E501,W291'
let g:ale_python_pylint_options = ''
```

当然也可以创建`~/.config/flake8`

```
[flake8]
ignore = E501,W291
```

### flake8 for python2

对于python2,需要指定执行路径

```vim
let g:ale_python_flake8_executable = 'python -m flake8'
```

另外，flake8适用的python版本不取决于安装flake8时pip的版本

> Installing Flake8 once will not install it on both Python 2.7 and Python 3.5. It will only install it for the version of Python that is running pip.

### pylint

- 生成pylint配置

```bash
pylint --generate-rcfile > ~/.pylintrc
```

- 编辑`~/.pylintrc`,找到**disable=**,在后面加上编号或message

```
disable=print-statement,
        ...
        C0301
```

- 配置vim

```vim
let g:ale_python_pylint_options = '--rcfile=~/.pylintrc'
```

### pylint for python2

当前的pylint版本是**2.1.1**，很多时候不适用于python2,比如

```python
print '...'
```

会显示syntax-error: invalid syntax,这时需要

- 使用pylint老版本,比如**1.9.3**

```bash
pip install pylint==1.9.3 --user
```

使用`--user`参数，避免与已安装的pylint(for python3)冲突。这时pylint的安装路径是`/home/theviper/.local/bin/pylint`,当然,需要把`/home/theviper/.local/bin/`加到**$PATH**

- 配置vim

```vim
let g:ale_python_pylint_executable = '/home/theviper/.local/bin/pylint'
```

---
如果配置了linter,但是没效果,可以`:ALEInfo`查看当前使用的linter,执行linter使用的参数,以及执行linter后的输出结果

## ale_fixers

> Suggested Fixers:   
  'add_blank_lines_for_python_control_statements' - Add blank lines before control statements.   
  'autopep8' - Fix PEP8 issues with autopep8.   
  'black' - Fix PEP8 issues with black.   
  'isort' - Sort Python imports with isort.   
  'remove_trailing_lines' - Remove all blank lines at the end of a file.   
  'trim_whitespace' - Remove all trailing whitespace characters at the end of every line.   
  'yapf' - Fix Python files with yapf.   

```vim
let g:ale_fixers = {
\ 'python': ['autopep8','yapf'],
\}
```

同样的，这样配置`ale_fixers`也无效

```vim
let b:ale_fixers = ['autopep8', 'yapf']
```

## overall

```vim
Bundle 'w0rp/ale'
let g:ale_linters = {
\   'go': ['vet', 'errcheck'],
\   'python': ['flake8', 'pylint'],
\}
let g:ale_python_flake8_options = '--ignore=E501,W291,N806,F405'

let g:ale_fixers = {
\ 'python': ['autopep8','yapf'],
\}

nnoremap <silent> <C-U> :ALEPreviousWrap<CR>
nnoremap <silent> <C-D> :ALENextWrap<CR>
```

---
不定期更新
<!-- /build:content -->
