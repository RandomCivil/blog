<!-- build:title -->leetcode tips<!-- /build:title -->
<!-- build:tags -->algorithm<!-- /build:tags -->
<!-- build:content -->
## leetcode tips
###  tips

- 在遍历时就max,min比较结果
- 返回的结果不计入空间花费
- python str.isdigit()
- 正面不好解决时，从反面思考，比如总数-局部
- 统计计数时，注意用到的变量是否在前面修改了值
- 限制范围不大时，考虑knapsack dp
- For后i是最后的值
- ord() chr()
- divmod(5,4)=(1,1)
- python二维数组构造

```python
# wrong
l=[[0]*m]*n
# right
l=[[0]*m for _ in range(n)]
```

- int('03') => 3,这样就不用考虑数字前几位是0，需要转换的问题了
- map(int,'123') => [1,2,3]
- 首次设值，最后一次设值

```python
        start = float('inf')
        end = 0
        for i in range(size):
            n, sn = nums[i], snums[i]
            if n != sn:
                start = min(start, i)
                end = max(end, i)

```

- tuple间比较

```python
>>> (3,'a')>(2,'b')
True
>>> (2,'a')>(2,'b')
False
>>> (2,'c')>(2,'b')
True
```

> Tuples are compared position by position: the first item of the first tuple is compared to the first item of the second tuple; if they are not equal (i.e. the first is greater or smaller than the second) then that's the result of the comparison, else the second item is considered, then the third and so on.

- 比较时注意条件是否是<=，而不是<
- 字符串组装，以使得相邻字符不相同.用counter高的字符和低的字符组合,如aaabbc => ababac

```python
        r = []
        c = Counter(barcodes)
        print(c)

        h = []
        for ch, freq in c.items():
            heapq.heappush(h, (-freq, ch))
        prev = None
        while len(h) > 0:
            freq, ch = heapq.heappop(h)
            if freq == 0:
                continue
            print(freq, ch, h)
            r.append(ch)
            if prev:
                pfreq, pch = prev
                heapq.heappush(h, (pfreq + 1, pch))
            prev = (freq, ch)
        return r

```



- itertools.zip_longest(*args, fillvalue=None)
- itertools.groupby

```python
text='aaabaaa'
g = [(k, len(list(v))) for k, v in itertools.groupby(text)]
# [('a', 3), ('b', 1), ('a', 3)]
```



- fibonacci

```python
x, y = y, x + y
```

- 字符串之间比较，比较的是字典序

```python
>>> 'ab'>'a'
True
>>> 'ab'>'aba'
False
```

更优方法

```python
        res = [0] * n
        for k, v in collections.Counter(barcodes).most_common():
            print(k, v)
            for _ in range(v):
                res[i] = k
                i += 2
                if i >= n:
                    i = 1
```

- all用法

```python
d = [(0, 'a'), (0, 'b')]
# true
a = all(c == 0 for c, ch in d)
```

- 打印可能导致大数据时，不能通过，虽然算法是正确的

- python变量名不要与参数相同，会覆盖，影响结果

- subsequence`依次`查找

    ```python
    str.find(sub[, start[, end]])
    
    def check_sub(q):
        i = -1
        for ch in pattern:
            i = q.find(ch, i + 1)
            print(q, ch, i)
            if i == -1:
                return False
            return True
    ```

- 优化后的brute force也是一种方法

- go one byte compare

```go
[]byte{s[i]}
```

- 一次循环得到max[i]-min[i]的最大值

```python
        index = [i for _, i in sorted_list]
        print(index)
        r = 0
        i = float("inf")
        for j in index:
            i = min(i, j)
            r = max(r, j - i)
```

- 负数%

```
>>> -2%5
3
>>> 0%5
0
```

- 同一对角线上的 i-j 值相同
- 循环不要修改变量，要另外定义一个变量
- 生成数字方式:arr拼接,**算术**
- dict之间比较:len相同 + k,v相同

```python
>>> c1=Counter('ab')
>>> c2=Counter('ab')
>>> c2['c']=1
>>> c1
Counter({'a': 1, 'b': 1})
>>> c2
Counter({'a': 1, 'b': 1, 'c': 1})
>>> c2['c']=0
>>> c1==c2
False
>>> del c2['c']
>>> c1==c2
True
>>> c2['b']=2
>>> c1==c2
False
```

- 删除指定字符

```python
str[i]=''
```



### dict

- 记录位置时，注意当前位置和dict位置比较

- 利用同一个key,后面会覆盖之前的特性

### 3sum模版

```python
        nums.sort()
        for i in range(size - 2):
            if i > 0 and nums[i] == nums[i - 1]:
                continue
            l, r = i + 1, size - 1
            while l < r:
                s = nums[i] + nums[l] + nums[r]
                if s > 0:
                    r -= 1
                elif s < 0:
                    l += 1
                else:
                    result.append([nums[i], nums[l], nums[r]])
                    # while l < r and nums[l] == nums[l + 1]:
                    while l + 1 < size and nums[l] == nums[l + 1]:
                        l += 1
                    # while l < r and nums[r] == nums[r + 1]:
                    while r - 1 >= 0 and nums[r] == nums[r - 1]:
                        r -= 1
                    l += 1
                    r -= 1
        return result

```



### combination sum

- unlimited

```go
for i, n := range candidates {
	if i < start {
		continue
	}
	if sum+n > target {
		break
	}
	path = append(path, n)
	dfs(i, path, sum+n)
	path = path[:len(path)-1]
}
```

- once

```go
for i := start; i < size; i++ {
	if i > start && candidates[i] == candidates[i-1] {
		continue
	}
	if candidates[i]+sum > target {
		break
	}
	path = append(path, candidates[i])
	dfs(i+1, sum+candidates[i], path)
	path = path[:len(path)-1]
}
```

### permute

- 有重复

```go
for i, n := range nums {
	if _, ok := set[i]; ok {
		continue
	}
	if _, ok := set[i-1]; ok && i > 0 && nums[i] == nums[i-1] {
		continue
	}
	set[i] = struct{}{}
	path = append(path, n)
	dfs(step+1, path)
	path = path[:len(path)-1]
	delete(set, i)
}

for i, n := range nums {
    if i > 0 && nums[i] == nums[i-1] {
        continue
    }
    if counter[n] <= 0 {
        continue
    }
    counter[n]--
    path = append(path, n)
    dfs(step+1, path)
    path = path[:len(path)-1]
    counter[n]++
}
```

### interval

- merge

```python
intervals.sort(key=lambda x: x[0])
r = [intervals[0]]
size = len(intervals)
for i in range(1, size):
    cs, ce = intervals[i]
    ls, le = r[-1]
    if le >= cs:
        r[-1][1] = max(ce, le)
    else:
        r.append(intervals[i])
return r
```

- 检查是否交叉

```python
        i = j = 0
        while i < len(A) and j < len(B):
            # Let's check if A[i] intersects B[j].
            # lo - the startpoint of the intersection
            # hi - the endpoint of the intersection
            lo = max(A[i][0], B[j][0])
            hi = min(A[i][1], B[j][1])
            if lo <= hi:
                ans.append([lo, hi])

            # Remove the interval with the smallest endpoint
            if A[i][1] < B[j][1]:
                i += 1
            else:
                j += 1

        return ans
```



### dp

- 考虑将dp\[i\]\[j\]设置为0
- 保存dp的数据结构可以是map,set,也可以是**多个数组**,甚至是几个**变量**
- 最顶值，最底值不一定是最终结果，过程中的值(min,max)也可以是结果
- 有时候，for的顺序对结果有影响,[518](https://leetcode.com/problems/coin-change-2/)
- 二维数组涉及第一行，列；最后一行，列，可以用两两比较，兼容非第一，最后行，列情况,[542](https://leetcode.com/problems/01-matrix/)

```go
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if mat[i][j] == 0 {
				dp[i][j] = 0
				continue
			}
			if j > 0 {
				dp[i][j] = Min(dp[i][j], dp[i][j-1]+1)
			}
			if i > 0 {
				dp[i][j] = Min(dp[i][j], dp[i-1][j]+1)
			}
		}
	}
```

- deleted characters to make two strings equal考虑lcs

```go
	dp := make([][]int, size1+1)
	for i := 0; i < size1+1; i++ {
		dp[i] = make([]int, size2+1)
	}

	for i := 1; i <= size1; i++ {
		for j := 1; j <= size2; j++ {
			if word2[j-1] == word1[i-1] {
				dp[i][j] = dp[i-1][j-1] + 1
			} else {
				dp[i][j] = Max(dp[i-1][j], dp[i][j-1])
			}
		}
	}
	// fmt.Println(dp)
	return size1 + size2 - dp[size1][size2]
```

- dp[(i,j)]=n,key是元组
- stone 1049

```python
        # dp保存所有有可能的subsequence和
    	dp = {0}
        sumA = sum(A)
        for a in A:
            dp |= {a + i for i in dp}
        return min(abs(sumA - i - i) for i in dp)
```

- subarray dp：考虑n在第一个位置和不在第一个位置这两种情况



### binary search

```go
for start<=end{
    mid :=(start+end)/2
    x :=mid/n
    y :=mid%n
    if target==matrix[x][y]{
        return true
    }

    if target<matrix[x][y]{
        end=mid-1
    }else{
        start=mid+1
    }
}
```

- 搜索方向不一定是水平垂直方向，可以是斜的
- lo,hi不一定是index,可以是**搜索的值**

```python
        m, n = len(matrix), len(matrix[0])
        lo, hi = matrix[0][0], matrix[m - 1][n - 1] + 1
        while lo <= hi:
            mid = (lo + hi) // 2
            count = 0
            for row in matrix:
                col = n - 1
                while col >= 0 and mid < row[col]:
                    col -= 1
                count += (col + 1)
                if col < 0:
                    break

            if count < k:
                lo = mid + 1
            else:
                hi = mid - 1

        return lo

```

- 有时lo<=hi不行时，试试lo<hi,hi = mid

### bst

分治法构造

```python
size=len(nums)
def divide(lo, hi):
    if lo>hi:
        return None
    if lo == hi:
        return TreeNode(nums[lo])
    mid = (lo + hi) // 2
    node = TreeNode(nums[mid])
    node.left = divide(lo, mid-1)
    node.right = divide(mid + 1, hi)
    return node

return divide(0,size-1)
```

### linked list

- find middle

```python
prevPtr = None
slowPtr = head
fastPtr = head

# Iterate until fastPr doesn't reach the end of the linked list.
while fastPtr and fastPtr.next:
    prevPtr = slowPtr
    slowPtr = slowPtr.next
    fastPtr = fastPtr.next.next
    
# slowPtr is middle
```

### dfs/bfs

- 剪支时，sum+head.val>target,注意正负数,0+(>-5时
- 遍历时可以修改值，节省后面遍历的次数
- 返回值后，停止后面搜索

```python
def dfs(cur, pro, visited):
    if cur == end:
        return pro

    if cur in visited:
        return None
    visited.add(cur)

    for nxt, val in d[cur]:
        v = dfs(nxt, pro * val, visited)
        if v:
            return v

        return None

```

- 搜索方向可以是parent
- 生成 Lexicographical字典序列考虑dfs
- bfs当前节点与子节点关系 deque=[(cur.left,2\*index+1),(cur.right,2\*index+2)]
- 不用对结果set去重的情况

```python
        c = Counter(tiles)
        print(c)
        nxt = list(c.keys())

        def dfs(cur, n):
            if len(cur) == n:
                print(cur, n)
                self.r += 1
                return
            for ch in nxt:
                if c[ch] == 0:
                    continue
                c[ch] -= 1
                dfs(cur + ch, n)
                c[ch] += 1

        for i in range(1, size + 1):
            dfs('', i)
        return self.r
```



### graph

- min height tree

```python
        if n <= 2:
            return [i for i in range(n)]

        neighbors = defaultdict(set)

        for s, e in edges:
            neighbors[s].add(e)
            neighbors[e].add(s)

        print(neighbors)
        leaves = [e for e, path in neighbors.items() if len(path) == 1]

        while n > 2:
            n -= len(leaves)
            next_leaves = []
            print(leaves)
            for leaf in leaves:
                s = neighbors[leaf].pop()
                neighbors[s].remove(leaf)
                if len(neighbors[s]) == 1:
                    next_leaves.append(s)

            leaves = next_leaves

        return leaves
```

- 拓扑排序
    1. 检测是否能完成所有课程,可处理有环的情况

```python
        count=0
        d = defaultdict(list)
        indegrees=defaultdict(int)
        for end, start in prerequisites:
            d[start].append(end)
            indegrees[end]+=1

        queue=deque()
        for i in range(numCourses):
            if indegrees[i]==0:
                queue.append(i)

        while len(queue)>0:
            cur=queue.popleft()
            count+=1
            for nxt in d[cur]:
                indegrees[nxt]-=1
                if indegrees[nxt]==0:
                    queue.append(nxt)

        if numCourses!=count:
            return False
        return True
```

		2. 输出满足依赖的结果，可处理有环的情况

```python
        d = defaultdict(list)
        indegrees = defaultdict(int)
        for end, start in prerequisites:
            d[start].append(end)
            indegrees[end] += 1

        queue = deque()
        for i in range(numCourses):
            if indegrees[i] == 0:
                queue.append(i)

        result = []
        while len(queue) > 0:
            cur = queue.popleft()
            result.append(cur)
            for nxt in d[cur]:
                indegrees[nxt] -= 1
                if indegrees[nxt] == 0:
                    queue.append(nxt)

        if numCourses != len(result):
            return []
        return result
```

- defaultdict存储邻接表，dfs时，**value pop()**,就不用visited记录访问过的节点
- Dijkstra

```python
        d = defaultdict(list)
        for _src, _dst, price in flights:
            d[_src].append((price, _dst))
        print(d)

        result = [float("inf")] * n
        result[src] = 0
        q = deque([(src, 0, 0)])
        while len(q) > 0:
            cur, price, count = q.popleft()
            if count > k:
                continue
            for p, nxt in d[cur]:
                if price + p < result[nxt]:
                    result[nxt] = price + p
                    q.append((nxt, price + p, count + 1))

        print(result)
        r = result[dst]
        return r if r < float('inf') else -1
```

- 注意考虑有环情况

- 检测是否有环:dfs + 着色

    1. White:unvisited
    2. Gray:visiting
    3. Black:visited

    ![](../images/graph-cycle.png)

### heap

```python
heap=[]
heapq.heappush(heap, item)
heapq.heappop(heap)
heapq.heapify(x)
heapq.nlargest(n, iterable, key=None)
heapq.nsmallest(n, iterable, key=None)
```

- 默认升序，若要降序，heapq.heappush(heap, -item)
- heappush,heappop操作后，会`保持`堆
- 可以利用tuple的比较机制，实现topK个(v1,v2)，若v1相等时，比较v2

```python
        c = Counter(words)
        l = [(-v, k) for k, v in c.items()]
        heapq.heapify(l)
        # why heappop?
        return [heapq.heappop(l)[1] for _ in range(k)]
```



### collections.Counter

- top k

```
Counter('abracadabra').most_common(3)
[('a', 5), ('b', 2), ('r', 2)]
```

- 也是defaultdict

```python
c=Counter()
c['d']+=2

# 0
c['a']
```

- subsequence不一定要Counter
- 运算

```python
>>> c1=Counter('ab')
>>> c2=Counter('bc')
>>> c1 and c2
Counter({'b': 1, 'c': 1})
>>> c1 + c2
Counter({'b': 2, 'a': 1, 'c': 1})
>>> c1 | c2
Counter({'a': 1, 'b': 1, 'c': 1})
>>> c1 & c2
Counter({'b': 1})
```

- elements()

```python
>>> c
Counter({'l': 2, 'e': 1})
>>> c.elements()
<itertools.chain object at 0x7fd084be81d0>
>>> list(c.elements())
['e', 'l', 'l']
```



### sort

sorted(iterable, *, key=None, reverse=False)

- The built-in [`sorted()`](https://docs.python.org/3/library/functions.html#sorted) function is guaranteed to be stable. A sort is stable if it **guarantees not to change the relative order** of elements that compare equal 

- \_\_lt\_\_自定义排序方式

```python
# <:升序 >:降序
class LargerNumKey(str):
    def __lt__(x, y):
        return x+y > y+x
        
class Solution:
    def largestNumber(self, nums):
        largest_num = ''.join(sorted(map(str, nums), key=LargerNumKey))
        return '0' if largest_num[0] == '0' else largest_num
```

- 多维度排序简化写法

```python
A.sort(key=lambda a: (a[0], -a[1]))
intervals = sorted(intervals, key=lambda x: (x[0], -x[1]))
```

- 列表里是字符串，sorted是按字典序排序

```python
>>> b
['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16']
>>> sorted(b)
['0', '1', '10', '11', '12', '13', '14', '15', '16', '2', '3', '4', '5', '6', '7', '8', '9']
```



### slide window

解决substring

- 模板1:j到达最后了，收缩可能会有满足条件的情况

minimal length of a **contiguous subarray** of which the sum >= target

```python
        s = 0
        i = 0
        for j in range(size):
            s += nums[j]
            while s >= target:
                result = min(result, j - i + 1)
                s -= nums[i]
                i += 1
        return 0 if result == float('inf') else result
```

- 模板2:最多k次操作(<=k)

You are given a string `s` and an integer `k`. You can choose any character of the string and change it to any other uppercase English character. You can perform this operation at most `k` times.

Return *the length of the longest substring containing the same letter you can get after performing the above operations*.

```python
        result = 0
        c = Counter()
        for i in range(size):
            c[s[i]] += 1
            mx = max(c.values())
            if result - mx < k:
                result += 1
            else:
                c[s[i - result]] -= 1

        return result
```



### bisect

a:无重复，有序列表

`bisect.bisect_left`(*a*, *x*, *lo=0*, *hi=len(a)*)

This module provides support for maintaining a list **in sorted order**

The returned insertion point *i* partitions the array *a* into two halves so that `all(val < x for val in a[lo:i])` for the left side and `all(val >= x for val in a[i:hi])` for the right side.

```
>>> a=[1,2,3,4,5]
>>> bisect.bisect_left(a,2)
1
>>> bisect.bisect_left(a,2.1)
2
>>> bisect.bisect_left(a,6)
5
>>> bisect.bisect_left(a,0)
0
>>> bisect.bisect_left(a,2,1)
1
>>> bisect.bisect_left(a,2,2)
2
```

### prefix sum

解决substring和

- dict[0]=1用于计数
- 不用单独一个for算出prefix sum

```python
        d = {0: -1}
        size = len(nums)
        prefix_sum = 0
        for i in range(size):
            prefix_sum += nums[i]
            mod = prefix_sum % k
            if mod in d:
                if i - d[mod] > 1:
                    return True
            else:
                d[mod] = i

        return False

```

- counter也可以prefix sum
- count0-count1=prev_count0-prev_count1 => count0-prev_count0=count1-prev_count1

### tree

- tree结构格式化

```python
        def dfs(root):
            if root is None:
                return '#'
            s = '{} {} {}'.format(root.val, dfs(root.left), dfs(root.right))

```



### 回文

- Palindromic Substrings.根据长度分3种情况讨论

```python
        // Base case: single letter substrings
        for (int i = 0; i < n; ++i, ++ans) 
            dp[i][i] = true;

        // Base case: double letter substrings
        for (int i = 0; i < n - 1; ++i) {
            dp[i][i + 1] = (s.charAt(i) == s.charAt(i + 1));
            ans += (dp[i][i + 1] ? 1 : 0);
        }

        // All other cases: substrings of length 3 to n
        for (int len = 3; len <= n; ++len)
            for (int i = 0, j = i + len - 1; j < n; ++i, ++j) {
                dp[i][j] = dp[i + 1][j - 1] && (s.charAt(i) == s.charAt(j));
                ans += (dp[i][j] ? 1 : 0);
            }

        return ans;
```

- 字符串的组合是回文的条件：至多有一个字符数量是1，其他字符数量均为偶数
- longest Palindromic Subsequece

```python
        dp = [[0] * size for _ in range(size)]
        for i in range(size):
            dp[i][i] = 1

        for i in range(size - 1):
            if s[i] == s[i + 1]:
                dp[i][i + 1] = 2
                result = max(result, 2)
            else:
                dp[i][i + 1] = 1

        for l in range(3, size + 1):
            for i in range(size - l + 1):
                j = i + l - 1
                if s[i] == s[j]:
                    dp[i][j] = max(dp[i][j], dp[i + 1][j - 1] + 2)
                else:
                    dp[i][j] = max(dp[i][j], dp[i][j - 1], dp[i + 1][j])
                result = max(result, dp[i][j])
        return result

```



### math

- 中位数

代表一个样本、种群或[概率分布](https://zh.wikipedia.org/wiki/概率分布)中的一个数值，其可将数值集合划分为数量相等的上下两部分

计算[有限](https://zh.wikipedia.org/wiki/有限)个数的[数据](https://zh.wikipedia.org/wiki/数据)的中位数的方法是：把所有的同类[数据](https://zh.wikipedia.org/wiki/数据)按照大小的顺序[排列](https://zh.wikipedia.org/wiki/排列)。如果数据的个数是[奇数](https://zh.wikipedia.org/wiki/奇数)，则中间那个[数据](https://zh.wikipedia.org/wiki/数据)就是这群[数据](https://zh.wikipedia.org/wiki/数据)的中位数；如果[数据](https://zh.wikipedia.org/wiki/数据)的个数是[偶数](https://zh.wikipedia.org/wiki/偶数)，则中间那2个[数据](https://zh.wikipedia.org/wiki/数据)的[算术平均值](https://zh.wikipedia.org/wiki/算术平均值)就是这群[数据](https://zh.wikipedia.org/wiki/数据)的中位数。

- (a-b)%k->a%k=b%k

### stack

- next greater number

```go
	result = make([]int, n)
	for i := n - 1; i >= 0; i-- {
		for len(stack) > 0 {
			top = stack[len(stack)-1]
			if arr[i] < top {
				result[i] = top
				break
			} else {
				stack = stack[:len(stack)-1]
			}
		}
		stack = append(stack, arr[i])
	}
```


---
不定期更新
<!-- /build:content -->
