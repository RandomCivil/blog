<!-- build:title -->Elastic Search搜索结果<!-- /build:title -->
<!-- build:content -->
<ul id='es-result'>
{% for _, item in ipairs(es_list) do %}
    <li>
        <div><a href="../{{item.path}}.html">{{item.title}}</a></div>
        {% for _, highlight in ipairs(item.Highlights) do %}
            <div>{\*highlight\*}</div>
        {% end %}
    </li>
{% end %}
</ul>
<!-- /build:content -->
