<!-- build:title -->博客开始支持HTTP3<!-- /build:title -->
<!-- build:tags -->blog<!-- /build:tags -->
<!-- build:content -->
参照`cloudflare`的[Experiment with HTTP/3 using NGINX and quiche](https://blog.cloudflare.com/experiment-with-http-3-using-nginx-and-quiche/),编译过程中可能会遇到很多问题，如果想省事，先体验尝鲜，可以使用**cdrocker/nginx:latest**这个docker镜像就行了

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: quiche
spec:
  updateStrategy:
    type: RollingUpdate
  selector:
    matchLabels:
      app: quiche
  template:
    metadata:
      labels:
        app: quiche
    spec:
      containers:
        - name: h3-nginx
          image: "cdrocker/nginx:latest"
          imagePullPolicy: Always
          ports:
            - name: quic
              containerPort: 4432
              hostPort: 443
              protocol: UDP
          volumeMounts:
            - name: cert-volume
              mountPath: /root/cert
            - name: http3-volume
              mountPath: /etc/nginx
      volumes:
        - name: cert-volume
          configMap:
            name: cert
        - name: http3-volume
          configMap:
            name: h3conf
```

这里使用`hostPort`暴露端口，注意`protocol`要显式声明为UDP

nginx.conf

```nginx
user root;
events {
    use epoll;
    worker_connections 1024;
}
http{
    charset utf-8;

    resolver kube-dns.kube-system.svc.cluster.local valid=5s;

    server{
        listen 4432 quic reuseport;
        server_name theviper.life;

        ssl_certificate /root/cert/theviper.life.cer;
        ssl_certificate_key /root/cert/theviper.life.key;

        ssl_protocols TLSv1.2 TLSv1.3;
        add_header alt-svc: 'alt-svc: h3-23=":443"; ma=86400';

        location / {
            proxy_pass http://blog:4430/;
        }
    }
}
```

创建configMap

```bash
kubectl create configmap h3conf --from-file=nginx.conf
kubectl create configmap cert --from-file=.acme.sh/theviper.life_ecc
```

[HTTP/3: the past, the present, and the future](https://blog.cloudflare.com/http3-the-past-present-and-future/#using-quiche-s-http3-client)里介绍了3种方法

1. Google Chrome Canary
2. curl
3. quiche http3-client

验证`http3`配置是否成功

第1种方法，反正我的Google Chrome Canary版本是怎么刷新也刷不出下面图中的Protocol:http2+quic/99

![](https://blog-cloudflare-com-assets.storage.googleapis.com/2019/09/Screen-Shot-2019-09-20-at-1.27.34-PM.png)

第2种方法[编译](https://github.com/curl/curl/blob/master/docs/HTTP3.md#quiche-version)有点麻烦   
第3种方法如果没有rust环境，可以使用构建好的docker镜像

```bash
docker run --rm cloudflare/quiche http3-client --no-verify https://theviper.xyz
```

<video controls preload='none'>
    <source src="https://theviper.xyz/videos/http3.webm" type="video/webm">
    Your browser does not support webm format video.
</video>

---
不定期更新
<!-- /build:content -->
