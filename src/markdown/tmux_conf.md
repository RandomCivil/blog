<!-- build:title -->我的tmux配置<!-- /build:title -->
<!-- build:tags -->tmux<!-- /build:tags -->
<!-- build:content -->
**fedora tmux 2.7**

`~/.tmux.conf`

```bash
unbind ^b
set -g prefix 'C-q'

unbind '"'
bind - splitw -v
unbind %
bind \ splitw -h

bind Escape copy-mode
setw -g mode-keys vi
bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-pipe 'xclip -in -selection clipboard'

bind -n C-h select-pane -L
bind -n C-j select-pane -D
bind -n C-k select-pane -U
bind -n C-l select-pane -R

is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
bind-key -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"

set-window-option -g xterm-keys on
set -g status-right ''
set -g default-terminal "xterm-256color"
```

## 快捷键

注意16行把**选择上面panel**的快捷键设置为`ctrl+K`,这个快捷键和`zsh`的**删除从光标位置到行尾间字符**的快捷键冲突，需要另外设置下,这里设置为`ctrl+S`

~/.zshrc

```
bindkey \^S kill-line
```

随便说一下，`zsh`的快捷键`ctrl+U`是删除整行，不是`bash`的**删除从光标位置到行首间字符**,可以将`ctrl+U`的行为修改为和`bash`一致

~/.zshrc

```
bindkey \^U backward-kill-line
```

要删除整行的话，可以先`ctrl+E`将光标移到行尾，再`ctrl+U`删除从光标位置(行尾)到行首间字符,也就是删除整行了

## macos复制粘贴

linux环境tmux的复制粘贴需要安装`xclip`,macos的话只需将`bind -T copy-mode-vi y send-keys -X copy-pipe`替换为

```
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "pbcopy"
```

---
不定期更新
<!-- /build:content -->
