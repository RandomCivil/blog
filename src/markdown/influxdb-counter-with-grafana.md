<!-- build:title -->influxdb counter with grafana<!-- /build:title -->
<!-- build:tags -->influxdb,grafana<!-- /build:tags -->
<!-- build:content -->
博客[迁移](/blog-migrate-to-kubernetes.html)到kubernetes,并且开了几个pod后,之前的[监控](/monitor-nginx-with-openresty-and-prometheus-1.html)就不能用了，因为kubernetes对pod的访问有[负载均衡](/kubernetes-nginx-ingress.html).prometheus scrape时,访问127.0.0.1:9145,每次的`instance`都不一样。   
可以使用prometheus的[pushgateway](https://github.com/prometheus/pushgateway),参见[WHEN TO USE THE PUSHGATEWAY](https://prometheus.io/docs/practices/pushing/)   
这里用influxdb,因为在之前文章[try influxdb with openresty in kubernetes](/try-influxdb-with-openresty-in-kubernetes.html)中,已经把供prometheus拉取的metrics数据也存入influxdb了,后面就是解决如何将influxdb里的数据展示成[prometheus counter](https://prometheus.io/docs/concepts/metric_types/#counter)这种效果

## Influx Query Language (InfluxQL)

### COUNT()

[COUNT() function](https://docs.influxdata.com/influxdb/v1.6/query_language/functions/#count)和sql类似,不过可以`group by时间间隔`,比如

```
SELECT count("url") FROM request WHERE type= 'other' AND $timeFilter GROUP BY time(1m)
```

![](../images/influxdb-count.png)

$timeFilter是grafana的内置变量，表示当前选定的时间范围的表达式，如选定Last 7 days,此时$timeFilter表示time > now() - 7d,上面的InfluxQL等价于

```
SELECT count("url") FROM request WHERE type= 'other' AND time > now() - 7d GROUP BY time(1m)
```

### CUMULATIVE_SUM()

上图很像[gauge](https://prometheus.io/docs/concepts/metric_types/#gauge),还不是counter效果。不过用influxdb提供的[CUMULATIVE_SUM() function](https://docs.influxdata.com/influxdb/v1.6/query_language/functions/#cumulative-sum),就能接近counter效果了。

```
SELECT cumulative_sum(count("url")) FROM request WHERE type= 'other' AND $timeFilter GROUP BY time(1m)
```

![](../images/influxdb-cumulative-sum.png)

然而y轴起始点为0,很尴尬

## grafana template variable

可以使用grafana的[template variable](http://docs.grafana.org/reference/templating)定义一个变量，表示y轴初始值,像这样

```
select count(*) from request where type='other' and time<now()-12h
```

![](../images/grafana-variables.png)

select时加上这个初始值

```
SELECT cumulative_sum(count("url"))+$other_init_count FROM request WHERE type= 'other' AND $timeFilter GROUP BY time(1m)
```

效果

![](../images/influxdb-grafana-variables.png)

## query $timeFilter

上面定义variable时,query里面的InfluxQL where语句对时间是写死的.可不可以用grafana的内置变量(如[$timeFilter](http://docs.grafana.org/reference/templating/#the-timefilter-or-timefilter-variable))替换呢?   
不可以,参见[Handle built-in variables (such as $timeFilter) in template variables](https://github.com/grafana/grafana/issues/9853)   

但是对于postgres,mysql,可以替换，参见

[mysql: pass timerange for template variable queries](https://github.com/grafana/grafana/pull/10071)   
[postgres: pass timerange for template variable queries](https://github.com/grafana/grafana/pull/10069)   

## time range value for template variable

其实即便influxdb支持variable query使用内置变量，也有问题，就是获取初始值的influxQL where语句需要的是`time<选定time range的开始时间`

```
select count(*) from request where type='other' and time<now()-12h
```

但是目前不能通过grafana内置变量获取选定time range的起始时间和结束时间,参见[Time range from & to as template variables](https://github.com/grafana/grafana/issues/1909).如果一定要获取,可以参考[链接](https://github.com/grafana/grafana/issues/1909),用angularjs获取time range部分的html,再从中提取起始结束时间

## temporary solution

我没有折腾angularjs那种方法，而是

- 另外定义了一个变量`range_start`

![](../images/grafana-range-start.png)

- 把之前获取初始值的influxQL改为

```
select count(*) from request where type='other' and time<now()-$range_start
```

- 后面每次改变time range时，range_start也对应改一下

![](../images/influxdb-counter-temporary-solution.png)

---
不定期更新
<!-- /build:content -->
