<!-- build:title -->转发ssh ipv6<!-- /build:title -->
<!-- build:tags -->iptables,linux<!-- /build:tags -->
<!-- build:content -->
[vultr](https://www.vultr.com/)最近推出$2.5的plan,不过只支持`ipv6`.如果集群节点的配置要求不高的话，可以用$2.5的机器做node   
当然如果你的网络不支持ipv6的话，需要有一台可以ipv4访问的机器做转发

## iptables nat forward

首先想到的是`iptables nat`转发,这里`eth0`是外网网卡，`forward_ip`是转发机器的外网ip(也就是eth0的ip),`remote_addr`是目标机器外网ip

`/etc/sysconfig/iptables`

```
*nat
:PREROUTING ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A PREROUTING -i eth0 -p tcp --dport 220 -j DNAT --to remote_addr:22
-A POSTROUTING -d remote_addr -p tcp -m tcp --dport 22 -j SNAT --to-source forward_ip
COMMIT

*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
...
-A FORWARD -m state --state ESTABLISHED -j ACCEPT
-A FORWARD -d remote_addr -j ACCEPT
-A FORWARD -j DROP
COMMIT
```

这种方式不能ipv4与ipv6相互转换，如上面配置的是ipv4的iptables,remote_addr我想转发到ipv6地址。iptables对于ipv4和ipv6的处理是相互独立的.  
不过如果有机器ip被封的话，可以用这种方式登录被封的机器

注意

- 打开ipv4转发

`/etc/sysctl.conf`

```
net.ipv4.ip_forward = 1
```

- 一定要设置`filter表的forward链`,见图,转自(https://wiki.archlinux.org/index.php/Iptables_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))

![](../images/iptables-process.png)

当linux收到了一个目的ip地址不是本地的包,如果开启转发的话，它将接着走入FORWARD链

- 一定要设置`-A FORWARD -m state --state ESTABLISHED -j ACCEPT`

表示对目标机器(外网)返回的包放行,因为当外网回包时,此时的状态是established

### local forward

如果是本地转发的话，直接

```
-A PREROUTING -p tcp --dport 8080 -j DNAT --to 127.0.0.1:80
```

不需要像上面一样，开启转发，设置`filter表的forward链`

## ssh forward

```
ssh -NTf -L 2121:[ipv6_addr]:22 forward_machine
```

ssh转发会在本地开一个端口,这里是2121,ssh连接的时候，连127.0.0.1:2121   
`~/.ssh/config`配置

```
Host forward_machine
    HostName forward_ip
    Port 22
    IdentityFile ~/.ssh/id_rsa
    User root
Host node
    HostName 127.0.0.1
    Port 2121
    IdentityFile ~/.ssh/id_rsa
    User root
```

这里直接`ssh node`就行了   
注意如果用的是`zsh`,`[ipv6_addr]`需要转义，写成`\[ipv6_addr\]`,否则zsh会把`[ipv6_addr]`当作`glob operators`,输出`zsh: no matches found`

## socat forward

如果不想本地开端口的话，可以用`socat`,在转发的机器上执行

```
socat TCP4-LISTEN:995,fork,su=nobody TCP6:[ipv6_addr]:22
```

ssh连接的时候，连转发机器的995端口就行了.这种方式不需要开启转发

> 参考   
[通过iptables实现端口转发和内网共享上网](http://xstarcd.github.io/wiki/Linux/iptables_forward_internetshare.html)

---
不定期更新
<!-- /build:content -->
