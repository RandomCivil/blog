<!-- build:title -->fedora workstation设置<!-- /build:title -->
<!-- build:tags -->linux<!-- /build:tags -->
<!-- build:content -->
## 启动盘

```bash
dd if=./ubuntu-iso-name.iso of=/dev/sdX
eject /dev/sdX
```

sdX为U盘的挂载点,可通过`dmesg | tail`查看

## rpmfusion

```bash
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```

## 无线网卡

```bash
dnf install kernel-devel akmod-wl
akmods
modprobe wl
systemctl restart NetworkManager
```

最后重启，就看到wifi的图标了

![](../images/fedora_wifi.png)

## tcp bbr

```bash
modprobe tcp_bbr 
echo "tcp_bbr" >> /etc/modules-load.d/modules.conf 
echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf 
sysctl -p 
```

`lsmod | grep bbr`出现bbr,则修改成功

## 修改dnf源

- 备份

```bash
mv /etc/yum.repos.d/fedora.repo /etc/yum.repos.d/fedora.repo.backup
mv /etc/yum.repos.d/fedora-updates.repo /etc/yum.repos.d/fedora-updates.repo.backup
```

- 下载dnf源

```bash
wget -O /etc/yum.repos.d/fedora.repo http://mirrors.aliyun.com/repo/fedora.repo
wget -O /etc/yum.repos.d/fedora-updates.repo http://mirrors.aliyun.com/repo/fedora-updates.repo
```

- 生成缓存

```bash
dnf makecache
```

也可以`dnf`设置代理,`vim /etc/dnf/dnf.conf`,添加

```conf
proxy=socks5://127.0.0.1:1080
```

## vim

- [vim-plug](https://github.com/junegunn/vim-plug)

```bash
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

```vim
call plug#begin('~/.vim/bundle')
Plug 'junegunn/fzf.vim'
...
call plug#end()
```

- 配置参见[我的vim配置](/vim_conf.html)

- [bat](https://github.com/sharkdp/bat)

```bash
dnf install bat
```

- [fd](https://github.com/sharkdp/fd)

```bash
dnf install fd-find
```

- [fzf](https://github.com/junegunn/fzf)

```bash
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
```

执行后,`~/.zshrc`会有

```bash
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
```

使用具体参见[命令行模糊搜索神器-fzf](/fzf-tips.html)

- [ag](https://github.com/ggreer/the_silver_searcher)

> A code-searching tool similar to ack, but faster

```bash
dnf install the_silver_searcher
```

- [YouCompleteMe](https://github.com/Valloric/YouCompleteMe)

```bash
dnf install clang automake gcc gcc-c++ kernel-devel cmake
dnf install python-devel python3-devel

cd ~/.vim/bundle/YouCompleteMe
./install.py --clang-completer --go-completer --system-libclang
```

安装时间可能有点长，`一定要耐心`，下载后没编译前,YouCompleteMe目录大小大概是237Mb   
如果只使用`python`补全,直接`./install.py`就行了.还要`golang`补全的话，`./install.py --go-completer`.需要`c/c++/objc`补全时,才会用到`clang`   
另外golang补全需要[gocode](https://github.com/nsf/gocode)或[godef](https://github.com/Manishearth/godef)   

> a combination of Gocode and Godef semantic engines for Go

安装一个即可

```bash
go get -u github.com/nsf/gocode
go get -u github.com/Manishearth/godef
```

还有`gopls`

```bash
GO111MODULE=on \
GOPATH=$PWD/third_party/ycmd/third_party/go \
GOBIN=$PWD/third_party/ycmd/third_party/go/bin \
go get golang.org/x/tools/gopls@v0.4.3
```

## zsh

```bash
dnf install zsh git util-linux-user
chsh -s $(which zsh)

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-history-substring-search ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-history-substring-search

plugins=(zsh-syntax-highlighting zsh-autosuggestions history-substring-search)
```

## tmux

```bash
dnf install tmux
```

配置参见[我的tmux配置](/tmux_conf.html)

## tilix

```bash
dnf install tilix
```

[tilix](https://github.com/gnunn1/tilix)就是linux上的`iterm`,支持分屏，还可以鼠标拖动改变分屏大小,终端内容也会自适应

## docker

```bash
wget https://download.docker.com/linux/fedora/27/x86_64/stable/Packages/docker-ce-18.06.1.ce-3.fc27.x86_64.rpm
dnf install docker-ce-18.06.1.ce-3.fc27.x86_64.rpm
```

设置代理   
`vim /usr/lib/systemd/system/docker.service`

```bash
Environment=ALL_PROXY=socks5://127.0.0.1:1080
```

## docker-compose

```bash
curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

## v2ray

```bash
docker pull v2ray/official
docker-compose up -d
```

```yaml
version: '3'
services:
  v2ray:
    container_name: "v2ray"
    image: "v2ray/official"
    ports:
      - "1080:1080"
    volumes:
      - ~/v2ray/config.json:/etc/v2ray/config.json
      - ~/v2ray:/var/log/v2ray
    network_mode: "host"
    restart: always
```

[规则列表](https://raw.githubusercontent.com/gfwlist/gfwlist/master/gfwlist.txt)

## git

- git代理

```bash
git config --global http.proxy 'socks5://127.0.0.1:1080'
git config --global https.proxy 'socks5://127.0.0.1:1080'

git config --global --unset http.proxy
git config --global --unset https.proxy
```

- git auth

```bash
ssh-keygen -t rsa -C "your email"
touch ~/.ssh/config && chmod 600 ~/.ssh/config
```

`~/.ssh/config`内容

```
Host gitlab.com
    IdentityFile ~/.ssh/git
```

## nodejs

```bash
wget https://nodejs.org/dist/v8.11.3/node-v8.11.3-linux-x64.tar.xz
xz -d https://nodejs.org/dist/v8.11.3/node-v8.11.3-linux-x64.tar.xz
tar -xvf https://nodejs.org/dist/v8.11.3/node-v8.11.3-linux-x64.tar.xz

export PATH=$PATH:/root/node-v8.11.3-linux-x64/bin
```

### npm global node_modules

```
mkdir npm-repo
npm config set prefix '/root/npm-repo'
export PATH=$PATH:/root/node-v8.11.3-linux-x64/bin:/root/npm-repo/bin
```

创建的npm-repo目录bin路径也要加入到PATH

### proxy

- 设置为taobao npm镜像

```
npm set registry https://registry.npm.taobao.org
```

- 或者设置http proxy

```
npm config set proxy http://127.0.0.1:1081
```

## 工具

```bash
dnf install htop vim tcpdump wireshark-gtk ack ctags xclip httpd-tools
```

vim的`ack.vim`全局搜索内容用到了`ack`,`tagbar`用到了`ctags`   
golang的tagbar需要

```bash
go get -u github.com/jstemmer/gotags
```

tmux vim模式复制粘贴需要xclip

### [autojump](https://github.com/wting/autojump)

```bash
git clone git://github.com/wting/autojump.git
cd autojump && ./install.py
```

`~/.zshrc`

```bash
plugins=(...autojump)

[[ -s /home/xzp/.autojump/etc/profile.d/autojump.sh ]] && source /home/xzp/.autojump/etc/profile.d/autojump.sh
autoload -U compinit && compinit -u
```

## pip3

```bash
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
```

国内镜像
~/.pip/pip.conf

```bash
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
```

## youtube download

```bash
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/sbin/youtube-dl
sudo chmod +x /usr/local/sbin/youtube-dl
```

## 输入法rime

[ibus-rime](https://github.com/rime/ibus-rime)是官方维护的，而[fcitx-rime](https://github.com/rime/librime)是第三方维护，这里用`ibus-rime`

```bash
dnf install ibus-rime
```

`ctrl+~`或`F4`选择rime方案

rime输入法下,按`SHIFT键`切换中英文,右上角的输入法图标不会改变

![](../images/rime-input.png)

还是使用`SUPER+空格`切换成系统的英文输入法

## gnome tweak tool

使用tweak tool可以很方便地设置gnome

![](../images/gnome-tweak-tool.png)

```bash
dnf install gnome-tweak-tool
```

## 终端中文乱码

`localectl`,看是否输出

```
System Locale: LANG=en_US.UTF-8
```

`vim /etc/environment`,添加

```
LC_ALL="en_US.UTF-8"
```

## 截图编辑工具shutter

```bash
dnf install shutter
```

## 浏览器

直接在fedora的软件中心安装`chrome`，`chrome`会是fedora维护的测试版，可能会有问题，比如在一些页面，滚动条无法滚动.还是

- 去[官网](https://www.google.cn/chrome/index.html)下载安装
- 或者添加google-chrome repo

```bash
cat <<EOF > /etc/yum.repos.d/google-chrome.repo
[google-chrome]
name=google-chrome
baseurl=http://dl.google.com/linux/chrome/rpm/stable/x86_64
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://dl.google.com/linux/linux_signing_key.pub
enabled=1
enabled_metadata=1
EOF
```

官网的是稳定版,而且内置了`mp4`解码器

`firefox`没有内置了`mp4`解码器，安装`ffmpeg`就可以解决.

```bash
dnf install ffmpeg
```

## smplayer

fedora自带的播放器对一些格式不支持，比如flv,smplayer功能很多，还能加载字幕

```bash
dnf install smplayer
```

## 录制屏幕

快捷键`Ctrl+Shift+Alt+R`开始或停止录制

```
gsettings set org.gnome.settings-daemon.plugins.media-keys max-screencast-length 1800
```

---
不定期更新
<!-- /build:content -->
