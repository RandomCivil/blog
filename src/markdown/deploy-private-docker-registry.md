<!-- build:title -->deploy private docker registry<!-- /build:title -->
<!-- build:tags -->docker<!-- /build:tags -->
<!-- build:content -->
## TLS+basic auth

- basic auth

```bash
dnf install httpd-tools
htpasswd -Bbn your_name your_pwd > /root/registry/htpasswd
```

- kubernetes

证书可以用[Let's Encrypt](https://letsencrypt.org/),这里registry用`DaemonSet`和`hostNetwork`部署在kubernetes

```yaml
apiVersion: v1
kind: Service
metadata:
  name: registry
spec:
  ports:
    - port: 5000
  selector:
    app: registry
---

apiVersion: extensions/v1beta1
kind: DaemonSet
metadata:
  name: registry
  labels:
    app: registry
spec:
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: registry
    spec:
      nodeSelector:
        name: hehe
      containers:
        - name: registry
          image: "registry:latest"
          imagePullPolicy: "IfNotPresent"
          env:
            - name: REGISTRY_AUTH_HTPASSWD_PATH
              value: "/auth/htpasswd"
            - name: REGISTRY_AUTH
              value: "htpasswd"
            - name: REGISTRY_AUTH_HTPASSWD_REALM
              value: "Registry Realm"
            - name: REGISTRY_HTTP_TLS_CERTIFICATE
              value: "/cert/your_domain.cer"
            - name: REGISTRY_HTTP_TLS_KEY
              value: "/cert/your_domain.key"
          ports:
            - containerPort: 5000
          volumeMounts:
            - name: storage
              mountPath: /var/lib/registry
            - name: auth
              mountPath: /auth/htpasswd
            - name: cert
              mountPath: /cert
      hostNetwork: true
      volumes:
        - name: storage
          hostPath:
            path: /root/registry/data
        - name: auth
          hostPath:
            path: /root/registry/htpasswd
        - name: cert
          hostPath:
            path: /root/registry/cert
```

注意

- 这种方式只能绑定443端口,上面绑定5000端口是因为在nginx上做了端口转发
- 需要信任证书

/etc/docker/daemon.json

```json
{
    "insecure-registries" : ["your_domain:your_port"]
}
```

这种方式的话，**不用显式**写上`:443`,直接["your_domain"]就行了,最后重启docker

## stunnel+plain HTTP

不能使用no TLS+basic auth这种搭配

> It’s not possible to use an insecure registry with basic authentication

basic auth可以换成stunnel的`psk(Pre-Shared Key)`,也可以使用`证书`提供安全的加密连接   
client->client stunnel(5000)->server stunnel(10001)->server docker registry(5000)

- 自签证书

```bash
openssl req -new -x509 -days 365 -nodes -out stunnel.pem -keyout stunnel.pem
```

- stunnel配置

server side

```
[registry]
accept = 10001
connect = 5000

cert=/etc/stunnel/stunnel.pem
CAfile=/etc/stunnel/stunnel.pem
verifyPeer=yes
```

client side

```
[registry]
client = yes
accept = 127.0.0.1:5000
connect = registry-ip:10001

cert=/etc/stunnel/stunnel.pem
```

- kubernetes

这里证书和basic auth都不用了

```yaml
apiVersion: v1
kind: Service
metadata:
  name: registry
spec:
  ports:
    - port: 5000
  selector:
    app: registry
---

apiVersion: extensions/v1beta1
kind: DaemonSet
metadata:
  name: registry
  labels:
    app: registry
spec:
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: registry
    spec:
      nodeSelector:
        name: hehe
      containers:
        - name: registry
          image: "registry:latest"
          imagePullPolicy: "IfNotPresent"
          ports:
            - containerPort: 5000
          volumeMounts:
            - name: storage
              mountPath: /var/lib/registry
      hostNetwork: true
      volumes:
        - name: storage
          hostPath:
            path: /root/registry/data
```

- docker proxy

1. /usr/lib/systemd/system/docker.service

```
...
Environment=HTTP_PROXY=http://127.0.0.1:5000
Environment=HTTPS_PROXY=http://127.0.0.1:5000
Environment=NO_PROXY=".docker.io,.docker.com"
...
```

2. /etc/docker/daemon.json

```json
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://127.0.0.1:5000",
     "httpsProxy": "http://127.0.0.1:5000",
     "noProxy": ".docker.io,.docker.com"
   }
 }
}
```

可能需要配置`noProxy`,因为有些镜像可能在其他地方，如[Docker Hub](https://hub.docker.com)   
最后需要重启docker

- 信任证书

/etc/docker/daemon.json

```json
{
    "insecure-registries" : ["your_domain:your_port"]
}
```

需要重启docker

---
不定期更新
<!-- /build:content -->
