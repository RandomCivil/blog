<!-- build:title -->sed take effect problem<!-- /build:title -->
<!-- build:tags -->linux<!-- /build:tags -->
<!-- build:content -->

让sed命令生效,可以

- 加上`-i参数`(edit files in place)
- shell重定向

```bash
sed '1,$d' error.log > error.log
```

## 两种方式效果一样？

分别执行

![](../images/sed-take-effect.png)

发现执行`sed重定向`后,文件的[inode](https://zh.wikipedia.org/zh-hans/Inode)没变   
执行`sed -i`后，文件的[inode](https://zh.wikipedia.org/zh-hans/Inode)变了，可以认为是重新创建了文件.

## 硬链接与软链接

虽说两种方式最终效果一样，都让sed命令生效了,但如果考虑到`链接`,就不一样了   
构建docker镜像时，常常使用[ln命令](https://zh.wikipedia.org/wiki/Ln_(Unix\)),将日志文件链接到标准输出(/dev/stdout),标准错误(/dev/stderr),这样在`docker logs`时，就可以同时看到stdout和stderr了，如nginx的Dockerfile

```dockerfile
# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log
```

这里用的是软链接,这时如果用volumes

```yaml
version: '3'
services:
  gateway:
    container_name: "nginx"
    image: "nginx:latest"
    volumes:
      - ~/logs:/usr/local/openresty/nginx/logs
    ports:
      - "443:443"
      - "80:80"
    restart: always
```

当执行`sed -i`日志后，再去`tail -f`看日志,发现日志后面没输出了，因为

> 原始文件被删除后，符号链接(软链接)将失效，访问软链接时，会提示找不到文件，但硬链接文件还在，而且还保存有原始文件的内容。

<!-- /build:content -->
