<!-- build:title -->我的kubernetes监控<!-- /build:title -->
<!-- build:tags -->kubernetes,grafana<!-- /build:tags -->
<!-- build:content -->
分享下我的grafana kubernetes监控,支持namespace与pod联动

## 效果

![](../images/grafana-k8s-1.png)
![](../images/grafana-k8s-2.png)
![](../images/grafana-k8s-3.png)
![](../images/grafana-k8s-4.png)

[gist](https://gist.github.com/RandomCivil/05a2fcad8562b059659af621734aeb21)

<script src="https://gist.github.com/RandomCivil/05a2fcad8562b059659af621734aeb21.js"></script>

---
不定期更新
<!-- /build:content -->
