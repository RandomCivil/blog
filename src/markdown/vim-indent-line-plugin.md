<!-- build:title -->vim indent line plugin<!-- /build:title -->
<!-- build:tags -->vim<!-- /build:tags -->
<!-- build:content -->
常用的vim缩进显示插件:[vim-indent-guides](https://github.com/nathanaelkane/vim-indent-guides),[indentLine](https://github.com/yggdroot/indentline)

## indentLine

indentLine是基于vim的conceal实现的，所以要用它，使用的vim得有这个功能才行.   
在编辑markdown文件时，`conceal`功能会让强迫症不能忍

normal mode

![](../images/vim-conceal.png)

visual mode

![](../images/vim-no-conceal.png)

下面配置可以解决这个问题

```vim
Bundle 'Yggdroot/indentLine'
autocmd Filetype markdown let g:indentLine_setConceal = 0
autocmd Filetype markdown set conceallevel=0
```

第2,3行缺一不可

## vim-indent-guides

```vim
Bundle 'nathanaelkane/vim-indent-guides'
let g:indent_guides_guide_size = 1
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_tab_guides=0
set list
set listchars=tab:\|\ 
```

- 第2行,默认的line宽度和shiftwidth一样，个人觉得太宽了
- 第4行,在Tab缩进的情况不显示vim-indent-guides的线
- 最后2行,在Tab缩进的情况以`|`代替,应对go代码的情况

---
不定期更新
<!-- /build:content -->
