<!-- build:title -->我的vim配置<!-- /build:title -->
<!-- build:tags -->vim<!-- /build:tags -->
<!-- build:content -->
```vim
let s:darwin = has('mac')
if s:darwin
  set clipboard=unnamed
else
  set clipboard=unnamedplus
endif

"fix backspace键无效
set backspace=indent,eol,start

let mapleader=','

set nocompatible
"检测文件类型
filetype off

"根据检测到的不同类型,加载对应插件
filetype plugin on

call plug#begin('~/.vim/bundle')
Plug '~/.fzf'
Plug 'junegunn/fzf.vim'
map <leader>a :Ag!<space>
nnoremap <C-p> :Files<Cr>
command! -bang -nargs=* Ag
  \ call fzf#vim#ag(<q-args>,
  \                 <bang>0 ? fzf#vim#with_preview('up:60%')
  \                         : fzf#vim#with_preview('right:50%:hidden', '?'),
  \                 <bang>0)

Plug 'scrooloose/nerdtree'
map <Leader>n :NERDTreeToggle<CR>
map <Leader>f :NERDTreeFind<CR>

Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'jiangmiao/auto-pairs'
Plug 'scrooloose/nerdcommenter'
map <Leader><space> <Plug>NERDCommenterComment<CR>

Plug 'tmhedberg/matchit', { 'for': ['html'] }
Plug 'docunext/closetag.vim', { 'for': ['html'] }
Plug 'morhetz/gruvbox'

Plug 'majutsushi/tagbar'
nmap <F8> :TagbarToggle<CR>
let g:tagbar_sort = 0
"autocmd VimEnter * nested :TagbarOpen
"autocmd BufEnter * nested :call tagbar#autoopen(0)
let g:tagbar_type_go = {
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
\ }

Plug 'Valloric/YouCompleteMe', { 'for': ['go','python'] }
nmap <C-G> :YcmDiags<CR>
"nnoremap <leader>d :YcmCompleter GoToDeclaration<CR>
"nnoremap <leader>g :YcmCompleter GoTo<CR>
autocmd FileType python nnoremap <leader>r :YcmCompleter GoToReferences<CR>
autocmd FileType python nnoremap <leader>d :YcmCompleter GoToDeclaration<CR>
let g:ycm_seed_identifiers_with_syntax=1
let g:ycm_key_list_select_completion = ['<c-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<c-p>', '<Up>']
let g:ycm_cache_omnifunc=0
let g:ycm_python_binary_path = '/usr/bin/python3'
"let g:ycm_python_binary_path = '/usr/bin/python'
let g:ycm_auto_hover=-1
nmap <leader><leader>i <plug>(YCMHover)

Plug 'airblade/vim-gitgutter'
nmap <Leader>u :GitGutterUndoHunk<CR>
nmap <Leader>p :GitGutterPreviewHunk<CR>
nmap [h :GitGutterPrevHunk<CR>
nmap ]h :GitGutterNextHunk<CR>
"let g:gitgutter_signs = 0

Plug 'tpope/vim-surround'
Plug 'Yggdroot/indentLine', { 'for': ['go','python'] }
set list
set listchars=tab:\|\ 

Plug 'christoomey/vim-tmux-navigator'
Plug 'easymotion/vim-easymotion'
let g:EasyMotion_smartcase = 1
nmap s <Plug>(easymotion-s2)
nmap t <Plug>(easymotion-t2)

map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)
let g:EasyMotion_startofline = 0

"set incsearch
map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)
"map  n <Plug>(easymotion-next)
"map  N <Plug>(easymotion-prev)
autocmd VimEnter * :EMCommandLineNoreMap <C-v> <Over>(paste)

Plug 'fatih/vim-go', { 'for': ['go'] }
autocmd FileType go nmap <leader>d :GoDef<CR>
autocmd FileType go nmap <leader>r :GoReferrers<CR>
nmap <leader>ds :GoDefStack<CR>
"nmap <leader><leader>i :GoInfo<CR>
nmap <leader><leader>r :GoRename<CR>
nmap <leader><leader>t :GoTest<CR>
nmap <leader>cc :GoCoverageToggle<CR>
"autocmd FileType go setlocal expandtab
let g:go_fmt_command = "goimports"

"let g:go_auto_sameids = 1

Plug 'w0rp/ale', { 'for': ['python','lua'] }
let g:ale_linters = {
\   'python': ['flake8', 'pylint'],
\   'lua': ['luac'],
\}
"let g:ale_python_flake8_executable = 'python -m flake8'
"let g:ale_python_pylint_executable = '/home/xzp/.local/bin/pylint'
let g:ale_python_flake8_options = '--ignore=E116,E226,E501,E741'
let g:ale_python_pylint_options = '--rcfile=/home/xzp/.pylintrc'
let g:ale_fixers = {
\ 'python': ['autopep8','yapf'],
\}
nnoremap <silent> <C-U> :ALEPreviousWrap<CR>
nnoremap <silent> <C-D> :ALENextWrap<CR>
nmap <leader>i :ALEFix<CR>

Plug 'tpope/vim-repeat'
Plug 'godlygeek/tabular'

Plug 'tpope/vim-fugitive'
map <leader><leader>d :Gdiff<CR>

Plug 'junegunn/gv.vim'
map <leader>g :GV<CR>
map <leader><leader>g :GV!<CR>

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='gruvbox'

Plug 'terryma/vim-multiple-cursors'
let g:multi_cursor_select_all_word_key = '<C-a>'

Plug 'plasticboy/vim-markdown', { 'for': ['md'] }
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0

Plug 'thinca/vim-quickrun', { 'for': ['go','python'] }
nmap <F9> :QuickRun<CR>
let g:quickrun_config = {
\   "_" : {
\       "outputter" : "buffer",
\   },
\}

Plug 't9md/vim-choosewin'
nmap = <Plug>(choosewin)

Plug 'andrejlevkovitch/vim-lua-format'
autocmd FileType lua nnoremap <buffer> <c-k> :call LuaFormat()<cr>
autocmd BufWrite *.lua call LuaFormat()
call plug#end()

noremap <leader>c "_d

nmap <C-F9> :tabe<CR>
nmap <C-F10> :tabp<CR>
nmap <C-F11> :tabn<CR>
nmap <C-F12> :tabclose<CR>

nnoremap [<space>  :<c-u>put! =repeat(nr2char(10), v:count1)<cr>
nnoremap ]<space>  :<c-u>put =repeat(nr2char(10), v:count1)<cr>

" 当光标一段时间保持不动了，就禁用高亮
autocmd cursorhold * set nohlsearch
" 当输入查找命令时，再启用高亮
noremap n :set hlsearch<cr>n
noremap N :set hlsearch<cr>N
noremap / :set hlsearch<cr>/
noremap ? :set hlsearch<cr>?
noremap * *:set hlsearch<cr>

"启动自动补全
filetype plugin indent on

"文件修改后自动载入
set autoread

"取消备份
set nobackup
set noswapfile

set cursorcolumn
set cursorline

"不启用鼠标
set mouse-=a

"总是显示状态栏
set laststatus=2

"在状态栏显示正在输入的命令
set showcmd

"左下角显示当前vim模式
set showmode
set number
set relativenumber

"括号配对情况,挑战并高亮一下匹配的括号
set showmatch
set matchtime=2

"搜索忽略大小写
set ignorecase
set textwidth=79

"自动缩进
set smartindent
set autoindent
set tabstop=4
set shiftwidth=4
set expandtab

autocmd Filetype lua,json,yml,yaml setlocal ts=2 sw=2 expandtab
autocmd Filetype json,markdown let g:indentLine_setConceal = 0
autocmd Filetype json,markdown set conceallevel=0

set encoding=utf-8
set helplang=cn
set termencoding=utf-8

"vimrc修改后自动加载
autocmd! bufwritepost .vimrc source %

"自动补全
"让vim的补全菜单行为与一般ide一致
set completeopt=longest,menu

"增强模式中的命令行自动完成
set wildmenu
set wildignore=*.o,*.pyc,*.class,*.so,*.zip,*.png,*.jpg,*.jpeg

"开启语法高亮
syntax enable
"允许用指定语法高亮配色方案替换默认方案
syntax on

set background=dark
colorscheme gruvbox
```

---
不定期更新
<!-- /build:content -->
