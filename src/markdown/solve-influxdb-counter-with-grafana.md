<!-- build:title -->solve influxdb counter with grafana<!-- /build:title -->
<!-- build:tags -->influxdb,grafana<!-- /build:tags -->
<!-- build:content -->
随着[6.0.0-beta1](http://docs.grafana.org/guides/whats-new-in-v6-0/)的发布,上一篇[influxdb counter with grafana](/influxdb-counter-with-grafana.html)里的问题可以解决了

## template variables query support $timeFilter

5.3.0-beta1已支持,参见[Release Notes v5.3.x](https://community.grafana.com/t/release-notes-v5-3-x/10244)

> InfluxDB: Support timeFilter in query templating for InfluxDB.[#12598](https://github.com/grafana/grafana/pull/12598)

可以定义一个template variables:$other_range_first_time

```
select first("copy_time") from request where type='other' and $timeFilter
```

表示time range内第一个请求发生的时间.这里对`copy_time字段`取第一个，而不是`time字段`是因为

```
select first("time") from request where type='other' and $timeFilter
```

不会有结果的,无语,只有额外定义一个字段，保存时间.   
注意`copy_time字段`的类型是**float**，所以写入时copy_time**不要加双引号**

```lua
local copy_time=ngx.time()..'000'
local http = require "resty.http"
local httpc = http.new()
local res, err = httpc:request_uri("http://influxdb.default.svc.cluster.local:8086/write?db=blog_stat", {
  method = "POST",
  body = 'request url="'..path..'",type="'..request_type..'",ip="'..forwarded_for..'",copy_time='..copy_time,
})
```

否则，`copy_time`类型是string,不好比较

![](../images/influxdb-string-compare.png)

搞定`copy_time`后，之前的起始值`$other_init_count`

```
select count(*) from request where type='other' and time<now()-$range_start
```

变为

```
select count(*) from request where type='other' and copy_time<$other_range_first_time
```

这样就不用每次修改time range后，都要选择其他的$range_start

## $__from and $__to built-in template variables

grafana现在支持新的内置变量`$__from`,`$__to`,表示time range的起始时间和结束时间。太感动了！这样就不用绕一圈了,上面的influxQL可以变为

```
select count(*) from request where type='other' and time<$__from
```

---
不定期更新
<!-- /build:content -->
