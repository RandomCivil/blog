<!-- build:title -->配置ubuntu 20.04<!-- /build:title -->
<!-- build:tags -->linux<!-- /build:tags -->
<!-- build:content -->
# 配置ubuntu 20.04

## 编译无线网卡

新电脑的无线网卡是realtek 8852,没有内置网卡驱动，需要手动编译

```bash
sudo apt-get update
sudo apt-get install make gcc linux-headers-$(uname -r) build-essential git

git clone https://github.com/lwfinger/rtw89.git -b v5
cd rtw89
make
sudo make install

#关闭secure boot
#This loads the module
sudo modprobe rtw89pci
```

## 手动安装docky

ubuntu 20.04没有docky apt源,需要手动安装

```bash
mkdir -p ~/Downloads/docky
cd ~/Downloads/docky

wget -c http://archive.ubuntu.com/ubuntu/pool/universe/g/gnome-sharp2/libgconf2.0-cil_2.24.2-4_all.deb
wget -c http://archive.ubuntu.com/ubuntu/pool/main/g/glibc/multiarch-support_2.27-3ubuntu1_amd64.deb
wget -c http://archive.ubuntu.com/ubuntu/pool/universe/libg/libgnome-keyring/libgnome-keyring-common_3.12.0-1build1_all.deb
wget -c http://archive.ubuntu.com/ubuntu/pool/universe/libg/libgnome-keyring/libgnome-keyring0_3.12.0-1build1_amd64.deb
wget -c http://archive.ubuntu.com/ubuntu/pool/universe/g/gnome-keyring-sharp/libgnome-keyring1.0-cil_1.0.0-5_amd64.deb

sudo apt-get install ./*.deb

wget -c http://archive.ubuntu.com/ubuntu/pool/universe/d/docky/docky_2.2.1.1-1_all.deb
sudo apt-get install ./docky_2.2.1.1-1_all.deb
```

---
不定期更新
<!-- /build:content -->
