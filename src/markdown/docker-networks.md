<!-- build:title -->docker networks<!-- /build:title -->
<!-- build:tags -->docker,iptables<!-- /build:tags -->
<!-- build:content -->
创建docker自定义网络

```bash
docker network create hehe
```

```json
{
        "Name": "hehe",
        "Id": "740d57cab248b4e460fdde496c2feb6dd466510c86f8becacc74135b0a5e77da",
        "Scope": "local",
        "Driver": "bridge",
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        ...
        "Containers": {
            "da54364326d949745e39049345e686f94d9fcf2f86d3d73104a527cb8db17a22": {
                "Name": "openresty",
                "EndpointID": "7f8fc0bb38a72247cba0bcbce9da928ce44bb227de931fb5f78ed980cc02ee9d",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            },
            "df38a6de157495d71a14c86e046b4f34c870144c6fca59d353ab5559c36f6b45": {
                "Name": "blog",
                "EndpointID": "0ef0171bae78098de2351aa45d9f9f65f845de23104290bcef932d5fb2322d7b",
                "MacAddress": "02:42:ac:12:00:03",
                "IPv4Address": "172.18.0.3/16",
                "IPv6Address": ""
            }
        }
    }
```

docker会自动创建对应网卡

```
172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 linkdown
172.18.0.0/16 dev br-740d57cab248 proto kernel scope link src 172.18.0.1
```

docker-compose配置

```yml
version: '3'
services:
  gateway:
    container_name: "openresty"
    image: "registry.gitlab.com/love_little_fat_cat/docker-openresty:latest"
    ports:
      - "80:80"
      - "443:443"
    networks:
      - hehe
    restart: always
  blog:
    container_name: "blog"
    image: "theviper/blog:latest"
    volumes:
      - ~/openresty/blog.conf:/usr/local/openresty/nginx/conf/nginx.conf
    networks:
      - hehe
    restart: always
networks:
  hehe:
    external: true
```

容器`openresty`,`blog`都使用了网络`hehe`,它们之间可以通过`服务名`访问,比如在容器`openresty`内

```bash
curl blog:8080
```

下面看看docker是如何端口映射的

## nat table

### PREROUTING chain

```
target     prot opt in     out     source               destination
DOCKER     all  --  any    any     anywhere             anywhere             ADDRTYPE match dst-type LOCAL
```

目标地址是`local`的请求，进入docker链

### docker chain

```
target     prot opt in               out     source               destination
RETURN     all  --  docker0          any     anywhere             anywhere
RETURN     all  --  br-740d57cab248  any     anywhere             anywhere
DNAT       tcp  --  !br-740d57cab248 any     anywhere             anywhere             tcp dpt:https to:172.18.0.3:443
DNAT       tcp  --  !br-740d57cab248 any     anywhere             anywhere             tcp dpt:http to:172.18.0.3:80
```

`br-740d57cab248`和上面网卡hehe的Id对应   
2,3行，目标地址是local，且从docker0,bridge网卡过来,return   
4,5行，不是从bridge网卡过来，且目标端口是80,443.`地址转换`为172.18.0.3

### POSTROUTING chain

```
target      prot opt in     out               source               destination
MASQUERADE  all  --  any    !docker0          172.17.0.0/16        anywhere
MASQUERADE  all  --  any    !br-740d57cab248  172.18.0.0/16        anywhere
MASQUERADE  tcp  --  any    any               172.18.0.3           172.18.0.3           tcp dpt:https
MASQUERADE  tcp  --  any    any               172.18.0.3           172.18.0.3           tcp dpt:http
```

2,3行，容器发出的流量，目标网卡不是容器网卡(eth0),继续

## filter table

### FORWARD chain

```
target            prot opt in     out     source               destination
DOCKER-USER       all  --  any    any     anywhere             anywhere
DOCKER-ISOLATION  all  --  any    any     anywhere             anywhere
ACCEPT            all  --  any    docker0  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER            all  --  any    docker0  anywhere             anywhere
ACCEPT            all  --  docker0 !docker0  anywhere             anywhere
ACCEPT            all  --  docker0 docker0  anywhere             anywhere
ACCEPT            all  --  any    br-740d57cab248  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER            all  --  any    br-740d57cab248  anywhere             anywhere
ACCEPT            all  --  br-740d57cab248 !br-740d57cab248  anywhere             anywhere
ACCEPT            all  --  br-740d57cab248 br-740d57cab248  anywhere             anywhere
```

2,3行，如果没有在docker-user,docker-isolation链中匹配，继续   
4,5行，目标网卡是docker0,进入docker链   
6,7行，对从docker0网卡向外发出，accept   
8,9行，目标网卡是bridge,进入docker链   
10,11行，对从bridge网卡向外发出，accept

### docker chain

```
target     prot opt in               out              source               destination
ACCEPT     tcp  --  !br-740d57cab248 br-740d57cab248  anywhere             172.18.0.3           tcp dpt:https
ACCEPT     tcp  --  !br-740d57cab248 br-740d57cab248  anywhere             172.18.0.3           tcp dpt:http
```

非docker网卡访问docker容器，且目标端口是80，443，accept

### DOCKER-ISOLATION chain

```
target     prot opt in              out              source               destination
DROP       all  --  br-740d57cab248 docker0          anywhere             anywhere
DROP       all  --  docker0         br-740d57cab248  anywhere             anywhere
RETURN     all  --  any             any              anywhere             anywhere
```

不同docker网卡之间不能相互通信

### DOCKER-USER chain

...

---
不定期更新
<!-- /build:content -->
