<!-- build:title -->我的loki查询面板<!-- /build:title -->
<!-- build:tags -->grafana,loki<!-- /build:tags -->
<!-- build:content -->
分享下我的loki查询面板,支持namespace与pod联动.namespace,pod的获取和[我的kubernetes监控](/kubernetes-monitor.html)中的一样

## 效果

![](../images/loki-grafana-panel.png)

[gist](https://gist.github.com/RandomCivil/b8c6b5ba7ae5a132ac53fde9c118a7d5)

<script src="https://gist.github.com/RandomCivil/b8c6b5ba7ae5a132ac53fde9c118a7d5.js"></script>

---
不定期更新
<!-- /build:content -->
