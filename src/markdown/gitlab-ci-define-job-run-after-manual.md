<!-- build:title -->gitlab CI defines job to run after manual job completes successfully<!-- /build:title -->
<!-- build:tags -->gitlab-CI-CD<!-- /build:tags -->
<!-- build:content -->
标题啥意思呢？举个例子，我博客的[gitlab ci配置](/gitlab-ci-cd-config.html)分为3个stage

```yaml
stages:
  - build
  - release
  - deploy
```

之前这3步都是手动触发,现在我想`release`(手动触发)成功后,不用手动触发,直接就`deploy`.   
如果去掉`deploy`的`when:manual`,`build`后还没手动触发`release`,`deploy`就开始执行了 

![](../images/gitlab-runner-pipeline.png)

怎么办？

- [allow_failure](https://gitlab.com/help/ci/yaml/README#allow_failure),在`release`加上`allow_failure: false`,阻塞pipeline后面的job.
- [when](https://gitlab.com/help/ci/yaml/README#when),在`deploy`加上`when: on_success`,保证只有`release`执行成功后，`deploy`才会执行。`when`的默认值是on_success，所以不加上也没关系

## jobs retry

需要注意的是点击`retry`,只会触发当前的job.比如，我retry了上面的release，即使release执行成功，也不会接着执行后面的deploy
<!-- /build:content -->
