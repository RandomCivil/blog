var through = require('through2'),
    PluginError = require('plugin-error'),
    fs = require('fs'),
    path = require('path'),
    thunkify = require('thunkify'),
    writeFileThunk=thunkify(fs.writeFile)
    readFileThunk = thunkify(fs.readFile),
    co = require('co');

const PLUGIN_NAME = 'blog-tags';

var root_path=path.resolve(__dirname),
    exports = {'instance':tags,'data':{}},
    all_tags={},
    title_path={};

function* gen_tags_json(all_tags){
    yield writeFileThunk(root_path+'/tags.json',JSON.stringify(all_tags))
}

function* gen_title_path_json(title_path){
    yield writeFileThunk(root_path+'/title_path.json',JSON.stringify(title_path))
}

function* gen_tags_html(tag_list){
    var html='<ul>\n';
    for(var i=0;i<tag_list.length;i++){
        html+="<li><a href='../tag/"+tag_list[i]+"'/>"+tag_list[i]+"</a></li>\n"
    }
    html+='</ul>\n'
    return html
}

function tags() {
    function process (file, enc, cb) {
        if (file.isStream()) {
            this.emit('error', new PluginError(PLUGIN_NAME, 'Streams aren\'t supported'));
        }

        if (file.isBuffer()) {
            var title_reg=/build:title.+>(.*?)</,
                tag_reg=/build:tags.+>(.*?)</,
                path_reg=/markdown\/(.+)\./,
                content=file.contents.toString(enc),

                title_result=title_reg.exec(content),
                tag_result=tag_reg.exec(content),
                path_result=path_reg.exec(file.history[0]);

            var tags,title,path
            if(title_result!=null){
                title=title_result[1]
            }
            if(path_result!=null){
                path=path_result[1]
                if(path=='es' || path=='tags'){
                    cb(null, file);
                    return
                }
                title_path[title]=path
            }
            if(tag_result!=null){
                var item={title:title,path:path}
                tags=tag_result[1].split(',')
                for(var i=0;i<tags.length;i++){
                    var posts=all_tags[tags[i]]
                    if(posts==undefined){
                        all_tags[tags[i]]=[item]
                    }else{
                        all_tags[tags[i]].push(item)
                    }
                }
            }
            file.contents = Buffer.from(file.contents, enc);
        }

        cb(null, file);
    }

    return through.obj(process).on('end',function(){
        co(function* () {
            yield [
                gen_tags_json(all_tags),
                gen_title_path_json(title_path),
            ]
            return yield gen_tags_html(Object.keys(all_tags))
        }).then(function(val){
            exports.data=val
        })
    })
}

module.exports = exports
