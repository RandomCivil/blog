var path = require('path'),
    root_path=path.resolve(__dirname,'..'),
    gulp = require('gulp'),
    less = require('gulp-less'),
    minifyCSS = require('gulp-csso'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    template = require('gulp-template-html'),
    markdown = require('gulp-markdown'),
    rename = require("gulp-rename"),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    cache = require('gulp-cached'),
    replace = require('gulp-replace'),
    tap = require('gulp-tap'),
    tags=require('../tags.js');

gulp.task('image', function(){
    return gulp.src(root_path+'/src/imgs/**')
    .pipe(gulp.dest(root_path+'/images'))
});

gulp.task('video', function(){
    return gulp.src(root_path+'/src/videos/**')
    .pipe(gulp.dest(root_path+'/dist/videos'))
});

gulp.task('css', function(){
    return gulp.src(root_path+'/src/css/*')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(concat('bundle.css'))
    .pipe(cache('css'))
    .pipe(gulp.dest(root_path+'/dist'))
    .pipe(reload({ stream:true }));
});

gulp.task('js', function(){
    return gulp.src(root_path+'/src/js/*')
    .pipe(uglify())
    .pipe(concat('bundle.js'))
    .pipe(cache('js'))
    .pipe(gulp.dest(root_path+'/dist'))
    .pipe(reload({ stream:true }));
});

gulp.task('tags',function(){
    return gulp.src([root_path+'/src/markdown/!(index)*'])
    .pipe(tags.instance())
    .pipe(gulp.dest(root_path+'/src/markdown'))
});

gulp.task('markdown',['tags'], function(){
    return gulp.src([root_path+'/src/markdown/*',root_path+'/src/template.html'])
    .pipe(tap(function(file){
        file.contents =Buffer.from('<!-- build:tag_list -->'+tags.data+'<!-- /build:tag_list -->'+file.contents.toString())
    }))
    .pipe(markdown())
    .pipe(template(root_path+'/src/template.html'))
    .pipe(cache('markdown'))
    .pipe(replace('${domain}', '..'))
    .pipe(tap(function(file){
        var name = path.parse(file.path).name,
            content='';
        if(name=='index' || name=='tags' || name=='es'){
            content =file.contents.toString().replace('<h1>首页</h1>', '').replace('<p>标签:<!-- build:tags --></p>', '')
        }else{
            content =file.contents.toString()
        }
        file.contents =Buffer.from(content)
    }))
    .pipe(rename({extname: ".html"}))
    .pipe(gulp.dest(root_path+'/html'))
    .pipe(reload({ stream:true }));
});

gulp.task('serve', ['image','video','css','js','markdown'], function() {
    browserSync({
        server: {
            baseDir: root_path
        },
        open:false
    });

    gulp.watch(root_path+'/src/css/*', ['css']);
    gulp.watch(root_path+'/src/js/*', ['js']);
    gulp.watch([root_path+'/src/markdown/*',root_path+'/src/template.html'], ['markdown'])
});

gulp.task('default', ['image','video','css','js','markdown']);
