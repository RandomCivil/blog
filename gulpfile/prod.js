var path = require('path'),
    root_path=path.resolve(__dirname,'..'),
    gulp = require('gulp'),
    less = require('gulp-less'),
    minifyCSS = require('gulp-csso'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    template = require('gulp-template-html'),
    markdown = require('gulp-markdown'),
    rename = require("gulp-rename"),
    rev=require('gulp-rev'),
    revRewrite = require('gulp-rev-rewrite'),
    replace = require('gulp-replace'),
    tap = require('gulp-tap'),
    tags=require('../tags.js'),
    dist_dir='/blog';

gulp.task('image', function(){
    return gulp.src(root_path+'/src/imgs/**')
    .pipe(gulp.dest(root_path+dist_dir+'/images'))
});

gulp.task('video', function(){
    return gulp.src(root_path+'/src/videos/**')
    .pipe(gulp.dest(root_path+dist_dir+'/videos'))
});

gulp.task('css', function(){
    return gulp.src(root_path+'/src/css/*')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(concat('bundle.css'))
    .pipe(rev())
    .pipe(gulp.dest(root_path+dist_dir+'/dist'))
    .pipe(rev.manifest('css-manifest.json'))
    .pipe(gulp.dest(root_path+'/build'))
});

gulp.task('js', function(){
    return gulp.src(root_path+'/src/js/*')
    .pipe(uglify())
    .pipe(concat('bundle.js'))
    .pipe(rev())
    .pipe(gulp.dest(root_path+dist_dir+'/dist'))
    .pipe(rev.manifest('js-manifest.json'))
    .pipe(gulp.dest(root_path+'/build'))
});

gulp.task('tags',function(){
    return gulp.src([root_path+'/src/markdown/!(index)*'])
    .pipe(tags.instance())
    .pipe(gulp.dest(root_path+'/src/markdown'))
});

gulp.task('markdown',['css','js','tags'],function(){
    var manifest = gulp.src(root_path+'/build/*');
    return gulp.src(root_path+'/src/markdown/*.md')
    .pipe(tap(function(file){
        file.contents =Buffer.from('<!-- build:tag_list -->'+tags.data+'<!-- /build:tag_list -->'+file.contents.toString())
    }))
    .pipe(markdown())
    .pipe(template(root_path+'/src/template.html'))
    .pipe(replace('${domain}', 'https://theviper.fun'))
    .pipe(tap(function(file){
        var name = path.parse(file.path).name;
        if(name=='index' || name=='tags' || name=='es'){
            content =file.contents.toString().replace('<h1>首页</h1>', '').replace('<p>标签:<!-- build:tags --></p>', '')
        }else{
            content =file.contents.toString().replace('<!-- build:comment -->','<script src="https://utteranc.es/client.js" repo="RandomCivil/comment" issue-term="pathname" theme="github-light" crossorigin="anonymous" async>')
        }
        file.contents =Buffer.from(content)
    }))
    .pipe(rename({extname: ".html"}))
    .pipe(revRewrite({ manifest }))
    .pipe(gulp.dest(root_path+dist_dir+'/html'))
});

gulp.task('default', ['image','video','markdown']);
